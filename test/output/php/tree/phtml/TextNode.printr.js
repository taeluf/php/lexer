Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Taeluf\PHTML
            [declaration] => namespace Taeluf\PHTML;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [namespace] => Taeluf\PHTML
                            [fqn] => Taeluf\PHTML\TextNode
                            [name] => TextNode
                            [extends] => \DOMText
                            [declaration] => class TextNode extends \DOMText
                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => tagName
                                                            [declaration] => string $tagName
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => is this node the given tag
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => is
                                            [return_types] => Array
                                                (
                                                    [0] => bool
                                                )

                                            [body] => if (strtolower($this->nodeName)==strtolower($tagName))return true;
return false;
                                            [declaration] => public function is(string $tagName): bool
                                        )

                                )

                        )

                )

        )

)
