# Example
This is a really simple example of a functional, tested grammar. They get much more complex.
- Get further grammar-writing instructions in @see_file(docs/GrammarWriting.md)
- Look at the commands available in @see_file(docs/GrammarCommands.md)


## StarterGrammar's Directives
Most grammars will have more directives & multiple traits to facilitate organization. This example only uses one trait.
```php
@file(code/Starter/OtherDirectives.php)
```

## StarterGrammar
See that directives are built during `onGrammarAdded()` from the traits.
```php
@file(code/Starter/StarterGrammar.php)
```

