<?php

namespace Tlf\Lexer\Php;

trait LanguageDirectives {

    protected $_language_directives = [
        'php_code'=>[
            'is'=>[
                ':php_close',
                ':separator',
                ':function',
                ':namespace',
                ':class',
                ':interface',
                // ':trait',
                ':expression',
            ],
        ],

        'strings'=>[
            'is'=>[
                ':string_single',
                ':string_double',
                ':heredoc',
            ],
        ],


        'string_backslash'=>[
            'start'=>[
                'match'=>'/\\\\.$/',
                'stop',
                'halt.all',
            ],
        ],

        'string_instructions'=>[
            'start'=>[
                'rewind 1',
                'buffer.clear',
                'forward 1',
                'then :string_backslash',
            ],
            'stop'=>[
                'previous.set string',
                'rewind 1',
                // 'buffer.clear',
                'directive.pop',
            ]
        ],

        'string_single'=>[
            'start'=>[
                'match'=>"'",
                'inherit :string_instructions.start',
                'then :string_single.stop'=>[
                    'start'=>[
                        'inherit :string_instructions.stop',
                    ]
                ],
            ],
            'stop'=>[
                'match'=>"'",
            ],
        ],

        'string_double'=>[
            'start'=>[
                'match'=>'"',
                'inherit :string_instructions.start',
                'then :string_double.stop'=>[
                    'start'=>[
                        'inherit :string_instructions.stop',
                    ]
                ],
            ],
            'stop'=>[
                'match'=>'"',
            ],
        ],


        'heredoc'=>[
            // also capturing nowdoc
            'start'=>[
                'match'=>'<<<',
                'then :heredoc_key'
            ],
            'stop'=>[
                'rewind 1',
                'stop',
            ],
        ],

        'heredoc_key'=>[
            'start'=>[
                'match'=>'/\'?([a-zA-Z\_0-9]+)\'?[^a-zA-Z0-9_]/',
                'rewind 1',
                'previous.set heredoc_key !'=>'_token:match 1',
            ],
            'stop'=>[
                'match !'=>'_lexer:previous heredoc_key',
                'previous.set string',
                // 'buffer.clear'
                'directive.pop',
                // 'print' => "\n\n\n---\n",
                // 'die !' => '_lexer:previous string',
            ]
        ],

        'php_close'=>[
            'start'=> [
                'match'=>'?>',
                'buffer.clear'=>true,
                // 'then []'=>[
                    'then :php_open',
                    'then :php_echo',
                    'then :html',
                // ],
            ],
        ],


        'php_open'=>[
            'start'=>[
                // 'match <?php',
                'match'=>'<?php',
                'token:clearBuffer',
                'then :php_code',
            ],
            'stop'=>true
        ],

        'php_echo'=>[
            'regex'=>'/\<\?=>)$/',
            'then'=>[
                ':expression'=>[
                    'then'=>[
                        ':separator',
                        ':php_close',
                    ],
                ],

            ],
        ],

        'expression'=>[
            //don't start if something else starts... how??
            'start'=>[
               'match'=>';',
               'buffer.clear',
               'stop'
            ]
        ],
        



        'varchars' => [
            'start'=>[
                'match'=>'/^[a-zA-Z0-9_]+[^a-zA-Z0-9_]$/',
                'rewind'=>1,
                'buffer.notin keyword',
                'stop',
                'previous.set'=>'php.varchars',
                'buffer.clear'
            ],
        ],

        'separator'=>[
            'is'=>[
                'docblock:/*',
                ':comment',
                ':whitespace',
            ],
        ],

        'block'=>[
            'start' => '{',
            'stop'=> [
                'match'=>'}',
                'lexer:unsetPrevious docblock'
            ]
        ],


    //close directives list
    ];

}

