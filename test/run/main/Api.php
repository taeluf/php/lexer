<?php

namespace Tlf\Lexer\Test\Main;

/**
 * For developing a new programming interface, possibly just a wrapper for the Lexer.
 */
class Api extends \Tlf\Tester {

    public function testMain(){
        $this->disable();
        // Lexer2 is being used in the cli
        return;
        $lexer = new \Tlf\Lexer2();
        // $lexer->enable_debugging($line_number_to_stop_on);
        $ast = $lexer->getAstFromFile('');
    }

}


