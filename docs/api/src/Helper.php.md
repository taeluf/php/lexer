<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Helper.php  
  
# class Tlf\Lexer\Helper  
  
See source code at [/src/Helper.php](/src/Helper.php)  
  
## Constants  
  
## Properties  
- `public array $grammars = [  
        'docblock'=>'Tlf\\Lexer\DocblockGrammar',  
        'php'=>'Tlf\\Lexer\\PhpGrammar',  
        'bash'=>'Tlf\\Lexer\\BashGrammar',  
    ];` Array of grammar classes  
  
## Methods   
- `public function getAstFromFile(string $file_path): \Tlf\Lexer\Ast` Create a   
- `public function get_lexer_for(string $language_ext): \Tlf\Lexer` Get a Lexer initialized with the correct grammars.  
  
- `public function get_lexer_for_file(string $file_path): \Tlf\Lexer`   
  
