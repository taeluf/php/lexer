<?php

namespace Tlf\Lexer\Test\Directives;

trait ClassIntegration {

    protected $_class_integration_tests = [

        'Integration.Class.Methods.WithNestedBlock'=>[
            'is_bad_test'=>'This test validates that we can handle nested blocks without messing up the ASTs.',
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>
                <<<PHP
                    class A {
                        public function is_the_us_imperialist():bool {
                            if (\$you_buy_into_imperialist_propaganda){
                                if (true){
                                    return false;
                                }
                            } 
                            return true;
                        } 
                        public function is_the_us_nice():bool {
                            if (\$you_are_rich){
                                if (true){
                                    return true;
                                }
                            } 
                            return false;
                        } 
                    } //
                PHP,
            'expect'=>[
                'class'=>[
                    0=>[
                        'type'=>'class',
                        'fqn'=>'A',
                        'namespace'=>'',
                        'name'=>'A',
                        'declaration'=>'class A',
                        'methods'=>[
                            0=>[
                                'type'=>'method',
                                'args'=>[],
                                'modifiers'=>['public'],
                                'name'=>'is_the_us_imperialist',
                                'return_types'=>['bool'],
                                'body'=>
                                    "if (\$you_buy_into_imperialist_propaganda){"
                                   ."\n    if (true){"
                                   ."\n        return false;"
                                   ."\n    }"
                                   ."\n}" 
                                   ."\nreturn true;",
                                'declaration'=>'public function is_the_us_imperialist():bool',
                            ],
                            1=>[
                                'type'=>'method',
                                'args'=>[],
                                'modifiers'=>['public'],
                                'name'=>'is_the_us_nice',
                                'return_types'=>['bool'],
                                'body'=>
                                    "if (\$you_are_rich){"
                                   ."\n    if (true){"
                                   ."\n        return true;"
                                   ."\n    }"
                                   ."\n}" 
                                   ."\nreturn false;",
                                'declaration'=>'public function is_the_us_nice():bool',
                            ],
                        ],
                    ]
                ]
            ],
        ],

        'Integration.Class.Final.Method'=>[
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>"final class Abc {\n"
                    ."    public function abc() {"
                    ."    }"
                    ."}",
            'expect'=>[
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'modifiers'=>['final'],
                    'declaration'=> 'final class Abc',
                    'methods'=>[
                        0=>[
                            'type'=>'method',
                            'args'=>[],
                            'modifiers'=>['public'],
                            'name'=>'abc',
                            'body'=>'',
                            'declaration'=>'public function abc()',
                        ],
                    ],
                    ],
                ],
            ],
        ],

        'Integration.Class.Bug.ModifiersMethods'=>[
            'is_bad_test'=>'This test works as intended, and was implemented to help handle a bug where a class\'s methods were being added to its modifiers. This was caused by getTree() recursion with arrays & use of the passthrough feature.',
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>"abstract class Abc {\n"
                ."    public function ",
            'expect'=>[
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'modifiers'=>['abstract'],
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'abstract class Abc',
                    'methods'=>[
                        0=>[
                            'type'=>'method',
                            'args'=>[],
                            'modifiers'=>['public'],
                        ],
                    ],
                    ],
                ],
            ],
        ],

        'Integration.Class.Method.2'=>[
            'ast.type'=>'file',
            'start'=>['php_code'],
            'input'=>"class Abc {\n"
                    ."    public function abc() {"
                    ."\n    }"
                    ."    private function def() {"
                    ."\n    }"
                    ."\n}"
                    ."final class Def {\n"
                    ."    public function abc() {"
                    ."\n    }"
                    ."    protected function def() {"
                    ."\n    }"
                    ."\n}"
                    ,
            'expect'=>[
                'namespace'=>'',
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'class Abc',
                    'methods'=>[
                            0=>[
                                'type'=>'method',
                                'args'=>[],
                                'modifiers'=>['public'],
                                'name'=>'abc',
                                'body'=>'',
                                'declaration'=>'public function abc()',
                            ],
                            1=>[
                                'type'=>'method',
                                'args'=>[],
                                'modifiers'=>['private'],
                                'name'=>'def',
                                'body'=>'',
                                'declaration'=>'private function def()',
                            ],
                        ],
                    ],
                    1=>[
                    'type'=>'class',
                    'fqn'=>'Def',
                    'namespace'=>'',
                    'name'=>'Def',
                    'modifiers'=>['final'],
                    'declaration'=> 'final class Def',
                    'methods'=>[
                            0=>[
                                'type'=>'method',
                                'args'=>[],
                                'modifiers'=>['public'],
                                'name'=>'abc',
                                'body'=>'',
                                'declaration'=>'public function abc()',
                            ],
                            1=>[
                                'type'=>'method',
                                'args'=>[],
                                'modifiers'=>['protected'],
                                'name'=>'def',
                                'body'=>'',
                                'declaration'=>'protected function def()',
                            ],
                        ],
                    ],
                ],
            ],
        ],

        'Integration.Trait.Method'=>[
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>"trait Abc {\n"
                    ."    public function abc() {"
                    ."    }"
                    ."}",
            'expect'=>[
                'trait'=>[
                    0=>[
                    'type'=>'trait',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'trait Abc',
                    'methods'=>[
                        0=>[
                            'type'=>'method',
                            'args'=>[],
                            'modifiers'=>['public'],
                            'name'=>'abc',
                            'body'=>'',
                            'declaration'=>'public function abc()',
                        ],
                    ],
                    ],
                ],
            ],
        ],

        'Integration.Class.Method'=>[
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>"class Abc {\n"
                    ."    public function abc() {"
                    ."    }"
                    ."}",
            'expect'=>[
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'class Abc',
                    'methods'=>[
                        0=>[
                            'type'=>'method',
                            'args'=>[],
                            'modifiers'=>['public'],
                            'name'=>'abc',
                            'body'=>'',
                            'declaration'=>'public function abc()',
                        ],
                    ],
                    ],
                ],
            ],
        ],
    ];
}
