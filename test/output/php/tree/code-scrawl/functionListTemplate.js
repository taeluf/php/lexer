{
    "type": "file",
    "namespace": "",
    "functions": [
        {
            "type": "function",
            "args": [],
            "name": "abc",
            "body": "",
            "declaration": "function abc()"
        },
        {
            "type": "function",
            "args": [],
            "name": "yep",
            "body": "",
            "declaration": "function yep()"
        }
    ],
    "class": [
        {
            "type": "class",
            "fqn": "Abc",
            "namespace": "",
            "name": "Abc",
            "declaration": "class Abc"
        },
        {
            "type": "class",
            "fqn": "Def",
            "namespace": "",
            "name": "Def",
            "declaration": "class Def"
        }
    ],
    "value": null,
    "comments": [
        "$descript = $method->description;"
    ]
}