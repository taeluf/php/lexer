{
    "type": "file",
    "namespace": "",
    "class": [
        {
            "type": "class",
            "fqn": "Sample",
            "namespace": "",
            "name": "Sample",
            "extends": "cats",
            "declaration": "class Sample extends cats",
            "methods": [
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "cats",
                    "body": "",
                    "declaration": "public function cats()"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "name": "arg",
                            "declaration": "$arg"
                        }
                    ],
                    "modifiers": [
                        "public"
                    ],
                    "name": "dogs",
                    "body": "",
                    "declaration": "public function dogs($arg)"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "name": "arg",
                            "value": "1",
                            "declaration": "$arg=1"
                        }
                    ],
                    "modifiers": [
                        "public"
                    ],
                    "name": "dogs2",
                    "body": "",
                    "declaration": "public function dogs2($arg=1)"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "name": "arg",
                            "value": "'yes'",
                            "declaration": "$arg='yes'"
                        }
                    ],
                    "modifiers": [
                        "public"
                    ],
                    "name": "dogs_3",
                    "body": "",
                    "declaration": "public function dogs_3($arg='yes')"
                },
                {
                    "type": "method",
                    "args": [],
                    "docblock": {
                        "type": "docblock",
                        "description": "bears are so cute"
                    },
                    "modifiers": [
                        "public"
                    ],
                    "name": "bears",
                    "body": "",
                    "declaration": "public function bears()"
                },
                {
                    "type": "method",
                    "args": [],
                    "docblock": {
                        "type": "docblock",
                        "description": "bears are so cute"
                    },
                    "modifiers": [
                        "public"
                    ],
                    "name": "bears_are_best",
                    "body": "",
                    "declaration": "public function bears_are_best()"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "bears_do_stuff",
                    "body": "echo \"love bears\";",
                    "declaration": "public function bears_do_stuff()"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "bears_cuddle_stuff",
                    "body": "$str = \"resist the temptation to cuddle a bear. not safe. big sad\";\necho $str;",
                    "declaration": "public function bears_cuddle_stuff()"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "bears_nest",
                    "body": "$cats = 'run away from bears';\nif ($cats == 'idk'){\n    echo \"this is a block\";\n}",
                    "declaration": "public function bears_nest()"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "name": "srcCode",
                            "declaration": "$srcCode"
                        }
                    ],
                    "docblock": {
                        "type": "docblock",
                        "description": "Replaces inline PHP code with placeholder, indexes the placeholder, and returns the modified code\n",
                        "attribute": [
                            {
                                "type": "attribute",
                                "name": "param",
                                "description": "mixed $srcCode - The source code"
                            },
                            {
                                "type": "attribute",
                                "name": "return",
                                "description": "string - source code with all PHP replaced by codeIds"
                            }
                        ]
                    },
                    "modifiers": [
                        "public"
                    ],
                    "name": "cleanSource",
                    "return_types": [
                        "string"
                    ],
                    "body": "$parser = new PHPParser($srcCode);\n$parsed = $parser->pieces();",
                    "declaration": "public function cleanSource($srcCode): string"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "makes_it_11",
                    "body": "",
                    "declaration": "public function makes_it_11()"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "output",
                    "return_types": [
                        "string"
                    ],
                    "body": "\/\/ print_r($this->code);\n\/\/ \/\/ echo $this->code[0]->;\n\/\/ exit;\n\/\/ print_r($this->placeholder);\n$code = implode(\"\\n\",$this->code);\n\/\/ return $code;\n$ph = [];\nforeach ($this->placeholder as $id=>$codeArray){\n    $ph[$id] = implode('',$codeArray);\n}\n$last = $code;\nwhile($last != $code = str_replace(array_keys($ph),$ph,$code))$last=$code;\nreturn $code;",
                    "declaration": "public function output(): string"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "ok_13",
                    "body": "",
                    "declaration": "public function ok_13()"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "arg_types": [
                                "string"
                            ],
                            "name": "file",
                            "declaration": "string $file"
                        },
                        {
                            "type": "arg",
                            "name": "chmodTo",
                            "value": "null",
                            "declaration": "$chmodTo=null"
                        }
                    ],
                    "modifiers": [
                        "public"
                    ],
                    "name": "writeTo",
                    "return_types": [
                        "bool"
                    ],
                    "body": "$output = $this->output();\nif (is_dir(dirname($file))){\n    mkdir(dirname($file),0771,true);\n    \/\/ chmod(dirname($file),0770);\n}\n$didPut = file_put_contents($file,$output);\nif ($chmodTo!==null){\n    \/\/ chmod($file,$chmodTo);\n}\nif ($didPut===false)return false;\nelse return true;",
                    "declaration": "public function writeTo(string $file, $chmodTo=null): bool"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "yep_15",
                    "body": "",
                    "declaration": "public function yep_15()"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "name": "html",
                            "declaration": "$html"
                        }
                    ],
                    "modifiers": [
                        "public"
                    ],
                    "name": "__construct",
                    "body": "parent::__construct();\n$this->srcHTML = $html;\n$parser = new PHTML\\PHPParser($html);\n$enc = $parser->pieces();\n$this->php = $enc->php;\n$this->cleanSrc = $enc->html;\n$this->cleanSrc = $this->cleanHTML($this->cleanSrc);\n$hideXmlErrors=true;\nlibxml_use_internal_errors($hideXmlErrors);\n$this->registerNodeClass('DOMElement', '\\\\Taeluf\\\\PHTML\\\\Node');\n$this->registerNodeClass('DOMText', '\\\\Taeluf\\\\PHTML\\\\TextNode');\n\/\/ $this->registerNodeClass('DOMText', 'RBText');\n$html = '<root>'.$this->cleanSrc.'<\/root>';\n$this->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);\n$this->formatOutput = true;\nlibxml_use_internal_errors(false);",
                    "declaration": "public function __construct($html)"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "okay_17",
                    "body": "",
                    "declaration": "public function okay_17()"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "name": "withPHP",
                            "value": "true",
                            "declaration": "$withPHP=true"
                        }
                    ],
                    "modifiers": [
                        "public"
                    ],
                    "name": "output2",
                    "body": "\/\/ echo \"\\n\".'-start output call-'.\"\\n\";\n$list = $this->childNodes[0]->childNodes;\n$hiddenTagsNodes = $this->xpath('\/\/*[@hideOwnTag]');\nforeach ($hiddenTagsNodes as $htn){\n    if ($htn->hideOwnTag==false||$htn->hideOwnTag=='false'){\n        unset($htn->hideOwnTag);\n        continue;\n    }\n    $parent = $htn->parentNode;\n    $childNodeList = $htn->children;\n    foreach ($childNodeList as $child){\n        $htn->removeChild($child);\n        $parent->insertBefore($child, $htn);\n    }\n    $parent->removeChild($htn);\n}\n$html = '';\nforeach ($list as $item){\n    $html .= $this->saveHTML($item);\n}\n\/** Run the php-code-replacer as long as there is a placeholder (while preventing infinite looping) *\/\n$html = $this->fill_php($html, $withPHP);\n$html = $this->restoreHtml($html);\nreturn $html;",
                    "declaration": "public function output2($withPHP=true)"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "now_19",
                    "body": "",
                    "declaration": "public function now_19()"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "name": "html",
                            "declaration": "$html"
                        },
                        {
                            "type": "arg",
                            "name": "withPHP",
                            "value": "true",
                            "declaration": "$withPHP=true"
                        }
                    ],
                    "modifiers": [
                        "public"
                    ],
                    "name": "fill_php",
                    "body": "$maxIters = 25;\n$iters = 0;\nwhile ($iters++<$maxIters&&preg_match('\/php([a-zA-Z]{26})php\/', $html, $match)){\n    foreach ($this->php as $id=>$code){\n        if ($withPHP)$html = str_replace($id,$code,$html);\n        else $html = str_replace($id,'',$html);\n    }\n}\nif (($phpAttrVal=$this->phpAttrValue)!=null){\n    $html = str_replace(\"=\\\"$phpAttrVal\\\"\", '', $html);\n}\nreturn $html;",
                    "declaration": "public function fill_php($html, $withPHP=true)"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "ugh_21",
                    "body": "",
                    "declaration": "public function ugh_21()"
                },
                {
                    "type": "method",
                    "args": [
                        {
                            "type": "arg",
                            "arg_types": [
                                "string"
                            ],
                            "name": "tableName",
                            "declaration": "string $tableName"
                        },
                        {
                            "type": "arg",
                            "arg_types": [
                                "array"
                            ],
                            "name": "colDefinitions",
                            "declaration": "array $colDefinitions"
                        },
                        {
                            "type": "arg",
                            "arg_types": [
                                "bool"
                            ],
                            "name": "recreateIfExists",
                            "value": "false",
                            "declaration": "bool $recreateIfExists=false"
                        }
                    ],
                    "modifiers": [
                        "public"
                    ],
                    "name": "create",
                    "body": "$colStatements = [];\nforeach ($colDefinitions as $col => $definition){\n    $statement = '`'.$col.'` '. $definition;\n    $colStatements[] = $statement;\n}\n$colsSql = implode(\", \", $colStatements);\n$drop = $recreateIfExists ? \"DROP TABLE IF EXISTS `{$tableName}`;\\n\" : '';\n$sql =\n<<<SQL\n    {$drop}\n    CREATE TABLE IF NOT EXISTS `{$tableName}`\n    (\n    {$colsSql}\n    )\n    ;\nSQL;\n$this->exec($sql);",
                    "declaration": "public function create(string $tableName, array $colDefinitions, bool $recreateIfExists=false)"
                },
                {
                    "type": "method",
                    "args": [],
                    "modifiers": [
                        "public"
                    ],
                    "name": "its_23",
                    "body": "",
                    "declaration": "public function its_23()"
                }
            ]
        }
    ]
}