<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Grammars/PhpGrammar.php  
  
# class Tlf\Lexer\Test\PhpGrammar  
  
  
## Constants  
  
## Properties  
- `protected $directive_tests;`   
  
## Methods   
- `public function prepare()`   
- `public function input_file($file)`   
- `public function testDirectives()` Unit test individual directives from the `test/src/Php/*.php` files  
  
For a summary of the directives, run `phptest -test testShowMePhpFeatures`, then see `test/output/PhpFeatures.md`  
- `public function testLilMigrationsBug()`   
- `public function testLilMigrations()`   
- `public function testLilDb()`   
- `public function testPhadFormsTest()`   
- `public function testScrawlFnTemplate()`   
- `public function testMethodParseErrors()`   
- `public function testPhtmlNode()`   
- `public function testPhtmlParser()`   
- `public function testPhtml()`   
- `public function testPhtmlCompiler()`   
- `public function testPhtmlTextNode()`   
- `public function testSampleClass()`   
- `public function parse_file($file, $debug, $stop_loop)` Get an ast tree from a file  
- `public function output_tree(string $file, array $tree)`   
- `public function get_counts($file)` get the expected counts from `test/php/counts/$file.json`  
- `public function assert_counts(array $ast_tree, array $target_counts)` Assert that the ast tree contains the given counts of items  
- `public function assert_file($file, bool $debug=true, int $stop_loop  -1)` Run tests on the given file. (currently just tree counts)  
  
1. Parses the input file into an ast tree  
2. Loads InputFile.expect.js, which contains things like the expected number of consts, methods, and comments  
3. Compares the output ast against the expected counts  
4. Writes the ast tree to `test/input/php/tree/{$file}.js` & `.../{$file.printr.js}`  
   - this is just for visual verification (i think)  
   - this should be changed to the output dir  
  
- `public function testShowMePhpFeatures()` This is not really a test.   
  
It writes file `test/output/PhpFeatures.md` showing a synopsis of which directives passed / failed & what their input was  
  
The output is like:  
 -FailedTestname: input string that was lexed  
 +PassedTestName: input string that was lexed  
- `public function testRunFile()` test an individual file (or all files in a directory) as needed  
  
- `public function testGetTreeCounts()` Test the test function  
- `public function get_tree_counts($array, $counts)` get an array of counts across an entire array.  
Each key increases by one when it is found,  
except when the value is an array containing numeric indices,  
then the count[key] increases by count(value)  
- `public function has_numeric_indices(array $array): bool`   
  
