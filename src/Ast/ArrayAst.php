<?php

namespace Tlf\Lexer;

class ArrayAst extends Ast {

    public function getTree($sourceTree = null){
        $val = $this->get('value') ?? [];
        foreach ($val as $i=>$v){
            if (is_object($v)){
                $val[$i] = $v->getTree();
            }
        }
        return $val;
    }

}
