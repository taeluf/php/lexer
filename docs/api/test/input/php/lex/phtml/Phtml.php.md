<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/php/lex/phtml/Phtml.php  
  
# class Taeluf\Phtml  
Makes DUMDocument... less terrible, but still not truly good  
  
## Constants  
  
## Properties  
- `protected string $src;` The source HTML + PHP code  
  
- `protected string $cleanSrc;` The source code with all the PHP replaced by placeholders  
- `protected array $php;` [ 'phpplaceholder' => $phpCode, 'placeholder2' => $morePHP ]  
- `protected $phpAttrValue;` A random string used when adding php code to a node's tag declaration. This string is later removed during output()  
  
## Methods   
- `public function __construct($html)` Create a DOMDocument, passing your HTML + PHP to __construct.   
  
  
- `public function placeholder(string $string): string`   
- `public function codeFromPlaceholder(string $placeholder): string` Get the code that's represented by the placeholder  
- `public function phpPlaceholder(string $enclosedPHP): string` Get a placeholder for the given block of code  
Intention is to parse a single '<?php //piece of php code ?>' and not '<?php //stuff ?><?php //more stuff?>'  
When used as intended, will return a single 'word' that is the placeholder for the given code  
  
- `public function fillWithPHP(string $codeWithPlaceholders): string` Decode the given code by replacing PHP placeholders with the PHP code itself  
  
- `public function __toString()` See output()  
  
- `public function output($withPHP=true)` Return the decoded document as as tring. All PHP will be back in its place  
  
- `public function fill_php($html, $withPHP=true)`   
- `public function xpath($xpath,$refNode=null)` get the results of an xpath query  
  
- `public function addPhpToTag($node, $phpCode)` Set an attribute that will place PHP code inside the tag declartion of a node.   
Basically: `<node phpCodePlaceholder>`, which pHtml will later convert to `<node <?='some_stuff'?>>`.   
This avoids problems caused by attributes requiring a `=""`, which `DOMDocument` automatically places.  
  
- `public function insertCodeBefore(\DOMNode $node, $phpCode)`   
- `public function insertCodeAfter(\DOMNode $node, $phpCode)`   
- `public function cleanHTML($html)`   
- `public function restoreHtml($html)`   
- `public function __get($param)`   
  
