<?php

namespace Tlf\Lexer;

class JsonAst extends Ast {

    public function getJsonData(){

        if ($this->type=='json'){
            $tree = $this->tree;
            $rootAst = $tree['root'] ?? null;
            if ($rootAst===null)return $rootAst;
            return $rootAst->getJsonData();
        } else if ($this->type=='array'){
            $root = [];
            foreach ($this->value as $k=>$v){
                if ($v instanceof \Tlf\Lexer\JsonAst){
                    $root[$k] = $v->getJsonData();
                } else if ($v instanceof \Tlf\Lexer\Ast){
                    $root[$k] = $v->getTree();
                } else {
                    $root[$k] = $v;
                }
            }
            return $root;
        } 

        throw new \Exception("\nWe don't currently handle output for '".$this->type."'");
    }

}
