<?php

namespace Tlf\Lexer\Test\Scrawl;

class Integrate extends \Tlf\Tester {




    public function testIdk(){
        $scrawl = new \Tlf\Scrawl2();
        $scrawl->dir_root = $this->file('');
        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);
        $ast = $php_ext->parse_file('test/run/Integrate.php');

        var_dump($ast);
        exit;
        print_r($ast->getTree());
        exit;
    }


    public function testGetAllClasses(){
        // goals:
        // 1. test all_classes template
        // 2. write api docs to disk for every php file in the code dirs, matching the directory structure on disk

        $class1 = <<<PHP
            <?php
            class Abc {
                function def(){}
            }
        PHP;
        $class2 = <<<PHP
            <?php
            class Ghi {
                function jkl(){}
            }
        PHP;
        $class3 = <<<PHP
            <?php
            class Mno{
                function pqr(){}
            }
        PHP;

        $scrawl = new \Tlf\Scrawl2();
        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);
        $ast1 = $php_ext->parse_str($class1);
        $ast2 = $php_ext->parse_str($class2);
        $ast3 = $php_ext->parse_str($class3);

        $php_ext->set_ast($ast1);
        $php_ext->set_ast($ast2);
        $php_ext->set_ast($ast3);

        $classes = $php_ext->get_all_classes();

        $this->compare(
            'Abc',
            $classes['Abc']['fqn'],
        );
        $this->compare(
            'Ghi',
            $classes['Ghi']['fqn'],
        );
        $this->compare(
            'Mno',
            $classes['Mno']['fqn'],
        );

    }







    public function testPhpExtWithScrawl(){
        echo "this is an old test from the beginning of the rewrite ... probably don't need it anymore. I don't think it was ever passing";
        $this->disable();
        return;
        $str = $this->php_code;
        $scrawl = new \Tlf\Scrawl2();

        $scrawl->extensions['code']['php'][] = new \Tlf\Scrawl\FileExt\Php();

        $res = $scrawl->parse_str($str, 'php');
        print_r($res);
        exit;

        print_r($scrawl->get('ast'));


        return;

        $scrawl->extensions['file']['php'];
        $scrawl->addExtension('file');

        $outputs = $scrawl->process_str($str, '.php');

        // this should have
        // ast = ... the ast ...
        // tags = 
        print_r($outputs);

        // i should use the lexer to build the ast explicitly
        $class_ast = null; 
        $method_ast = null;
        $this->compare(
            [
                'ast'=>[
                    'class'=>['Abc'=>$class_ast] // ast as from lexer
                ],
                'tags'=>[
                    'feature'=>['name'=>'no feature', 'target'=>'ast.class.Abc']
                ],
                'flat'=>[
                    'ast.class.Abc'=>$class_ast,
                    'ast.class.Abc.method.ghi'=>$method_ast,
                ],
            ],
            $outputs
        );

        return;
        // this shouldn't do anything bc i haven't added any extensions
        print_r($scrawl->getOutputs());
    }

}
