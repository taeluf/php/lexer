<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/old/php-new/StarterGrammar.16jul21.php  
  
# class Tlf\Lexer\Test\StarterGrammar_16Jul21  
An extremely simple grammar that builds sets of arglist from `(arg1,arg2,c) (list2_arg1,arg2)`  
  
## Constants  
  
## Properties  
- `protected $directives;` The actual array of directives, built during onGrammarAdded()  
  
## Methods   
- `public function getNamespace()` Defaults to 'startergrammar'  
- `public function buildDirectives()` Combine the directives from traits  
- `public function onGrammarAdded(\Tlf\Lexer $lexer)`   
- `public function onLexerStart(\Tlf\Lexer $lexer,\Tlf\Lexer\Ast $ast,\Tlf\Lexer\Token $token)`   
- `public function trimBuffer(\Tlf\Lexer $lexer, \Tlf\Lexer\Ast $ast, \Tlf\Lexer\Token $token, \stdClass $directive, array $args)` A method this grammar uses as an instruction to trim() the buffer  
  
