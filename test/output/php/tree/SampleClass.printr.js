Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Cats\Whatever
            [declaration] => namespace Cats\Whatever;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [docblock] => Array
                                (
                                    [type] => docblock
                                    [description] => This is the best class anyone has ever written.
                                )

                            [namespace] => Cats\Whatever
                            [fqn] => Cats\Whatever\Sample
                            [name] => Sample
                            [extends] => cats
                            [declaration] => class Sample extends cats
                            [comments] => Array
                                (
                                    [0] => First comment
                                    [1] => Second Comment
                                )

                            [properties] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Why would you name a giraffe Bob?
                                                )

                                            [name] => giraffe
                                            [value] => "Bob"
                                            [declaration] => protected $giraffe = "Bob";
                                        )

                                    [1] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => private
                                                )

                                            [name] => cat
                                            [value] => "Jeff"
                                            [declaration] => private $cat = "Jeff";
                                        )

                                    [2] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => static
                                                    [1] => public
                                                )

                                            [name] => dog
                                            [value] => "PandaBearDog"
                                            [declaration] => static public $dog = "PandaBearDog";
                                        )

                                )

                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => a
                                                            [value] => "abc"
                                                            [declaration] => $a= "abc"
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => dogs

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => dogs
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => dogs
                                            [body] => echo "yep yep";
                                            [declaration] => public function dogs($a= "abc")
                                        )

                                )

                            [const] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => const
                                            [name] => Doygle
                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [value] => "Hoygle Floygl"
                                            [declaration] => public const Doygle = "Hoygle Floygl";
                                        )

                                )

                        )

                )

        )

)
