<?php

namespace Tlf\Lexer\Ast;

class DocblockAst extends \Tlf\Lexer\Ast {

    public function getTree($sourceTree = null){
        $val = $this->get('value') ?? [];
        foreach ($val as $i=>$v){
            if (is_object($v)){
                $val[$i] = $v->getTree();
            }
        }
        return $val;
    }


    public function getCode(string $language): string{
        //var_dump($this);
        //exit;

        $code = '';
        switch($language){
            case 'php':
                $code = $this->get_php_code();
                break;
            case 'javascript': 
                $code = $this->get_javascript_code();
                break;
            default:
                throw new \Exception("Language '$language' is not valid");
        }
        return $code;
    }

    public function get_php_code(): string {
        $t = (object)$this->_tree;

        $lines = [];

        $lines[] = $t->description;

        $code = 
            "\n/**\n * "
            .implode("\n * ", $lines)
            ."\n */";

        return $code;
    }

    public function get_javascript_code(): string {

        return $this->get_php_code();
    }

}
