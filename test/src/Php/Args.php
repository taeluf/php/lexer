<?php

namespace Tlf\Lexer\Test\Directives;

trait Args {
    protected $_arg_tests = [
        'Arg.Assign.Concat.BeforeComma'=>[
            'ast.type'=>'method_arglist',
            'start'=>['php_code'],
            'input'=>' $global_warming = "angers"."me"."greatly", int $okay)',
            'expect'=>[
                'value'=>[
                    0=>[
                        'type'=>'arg',
                        'name'=>'global_warming',
                        'declaration'=>'$global_warming = "angers"."me"."greatly"',
                        'value'=> '"angers"."me"."greatly"',
                    ],
                    1=>[
                        'type'=>'arg',
                        'arg_types'=>['int'],
                        'name'=>'okay',
                        'declaration'=>'int $okay',
                    ]
                ],
            ],
        ],
        'Arg.Assign.Concat'=>[
            'ast.type'=>'method_arglist',
            'start'=>['php_code'],
            'input'=>' $global_warming = "angers"."me"."greatly")',
            'expect'=>[
                'value'=>[
                    0=>[
                        'type'=>'arg',
                        'name'=>'global_warming',
                        'declaration'=>'$global_warming = "angers"."me"."greatly"',
                        'value'=> '"angers"."me"."greatly"',
                    ],
                ],
            ],
        ],
        'Arg.Two'=>[
            'ast.type'=>'method_arglist',
            'start'=>['php_code'],
            'input'=>' string $blm = "abc", bool $dog = true )',
            'expect'=>[
                'value'=>[
                    0=>[
                        'type'=>'arg',
                        'arg_types'=>['string'],
                        'name'=>'blm',
                        'declaration'=>'string $blm = "abc"',
                        'value'=>'"abc"',
                    ],
                    1=>[
                        'type'=>'arg',
                        'arg_types'=>['bool'],
                        'name'=>'dog',
                        'declaration'=>'bool $dog = true',
                        'value'=>'true',
                    ],
                ],
            ],
        ],
        'Arg.Assign'=>[
            'ast.type'=>'method_arglist',
            'start'=>['php_code'],
            'input'=>' string $blm = "abc" )',
            'expect'=>[
                'value'=>[
                    0=>[
                        'type'=>'arg',
                        'arg_types'=>['string'],
                        'name'=>'blm',
                        'declaration'=>'string $blm = "abc"',
                        'value'=>'"abc"',
                    ],
                ],
            ],
        ],

        'Arg.Typed'=>[
            'ast.type'=>'method_arglist',
            'start'=>['php_code'],
            'input'=>' string $blm )',
            'expect'=>[
                'value'=>[
                    0=>[
                        'type'=>'arg',
                        'arg_types'=>['string'],
                        'name'=>'blm',
                        'declaration'=>'string $blm',
                    ],
                ],
            ],
        ],

        'Arg.Simple'=>[
            'ast.type'=>'method_arglist',
            'start'=>['php_code'],
            'input'=>'$blm)',
            'expect'=>[
                'value'=>[
                    0=>[
                        'type'=>'arg',
                        'name'=>'blm',
                        'declaration'=>'$blm',
                    ],
                ],
            ],
        ],
    ];
}
