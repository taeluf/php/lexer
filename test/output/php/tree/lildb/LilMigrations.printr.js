Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Tlf
            [declaration] => namespace Tlf;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [docblock] => Array
                                (
                                    [type] => docblock
                                    [description] => A minimal class for handling sql migration. Create a migrations dir. Then create dirs like `v1`, `v2` & create files `up.sql`, `down.sql` in each versioned dir. Migrating from 1 to 2 will execute `v2/up.sql`. From 3 down to 1 will execute `v2/down.sql` and `v1/down.sql`. You may also make files like `v1/up-1.sql`, `v1/up-2.sql` to execute multiple files in order.

                                    [attribute] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => attribute
                                                    [name] => tagline
                                                    [description] => Easy to use SQL Migrations from versioned directories
                                                )

                                        )

                                )

                            [namespace] => Tlf
                            [fqn] => Tlf\LilMigrations
                            [name] => LilMigrations
                            [declaration] => class LilMigrations
                            [properties] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                    [1] => \PDO
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => a pdo instance
                                                )

                                            [name] => pdo
                                            [declaration] => public \PDO $pdo;
                                        )

                                    [1] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                    [1] => string
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => the dir for migrations scripts.
                                                )

                                            [name] => dir
                                            [declaration] => public string $dir;
                                        )

                                )

                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => \PDO
                                                                )

                                                            [name] => pdo
                                                            [declaration] => \PDO $pdo
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => dir
                                                            [declaration] => string $dir
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => In $dir, there should be directories named 'v1', 'v2', 'v3', and so on.
In the v1/v2/v3 dirs, there should be up.sql & down.sql files with valid SQL statements for whatever database you're using

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $pdo a pdo instance
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $dir a directory path
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __construct
                                            [body] => $this->pdo = $pdo;
$this->dir = $dir;
                                            [declaration] => public function __construct(\PDO $pdo, string $dir)
                                        )

                                    [1] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => dbName
                                                            [value] => ':memory:'
                                                            [declaration] => string $dbName = ':memory:'
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Convenience method to initialize sqlite db in memory
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => Tlf\LilDb
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => static
                                                    [1] => public
                                                )

                                            [name] => sqlite
                                            [body] => $pdo = new \PDO('sqlite:'.$dbName);
$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
$ldb = new static($pdo);
return $ldb;
                                            [declaration] => static public function sqlite(string $dbName = ':memory:')
                                        )

                                    [2] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => int
                                                                )

                                                            [name] => old
                                                            [declaration] => int $old
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => int
                                                                )

                                                            [name] => new
                                                            [declaration] => int $new
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Migrate from old version to new

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $old the current version of the database
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $new the new version of the database to go to
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => migrate
                                            [body] => if ($old < $new){
    for ($i=$old+1; $i <= $new; $i++){
        $this->run_migration_version($i, 'up');
    }
} else if ($old > $new) {
    for ($i=$old-1; $i >= $new; $i--){
        $this->run_migration_version($i, 'down');
    }
} else {
    $this->run_migration_version($i, 'up');
}
                                            [declaration] => public function migrate(int $old, int $new)
                                        )

                                    [3] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => version
                                                            [declaration] => $version
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => up_or_down
                                                            [declaration] => $up_or_down
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => run_migration_version
                                            [body] => $i = $version;
$file = $up_or_down;
$v_dir = "v$i/";
$migrate_dir = $this->dir.'/'.$v_dir;
$files = is_dir($migrate_dir) ? scandir($migrate_dir) : false;
if ($files==false){
    echo "\nMigrations dir $v_dir does not exist. Continuing.";
    return;
}
$files = array_filter($files,
    function($v) use ($file){
        if (substr($v,0,strlen($file))==$file)return true;
        return false;
    });
//@todo test the file sorting
// // for testing
// rsort($files);
//
// $files = ['up-9.sql', 'up-2.sql', 'up-13.sql', 'up-0.sql', 'up.sql', 'up-1.sql'];
usort($files,
    function($v1, $v2){
        $pos1 = strpos($v1,'-');
        if ($pos1==false)$index1 = -1;
        else $index1 = (int)substr($v1,$pos1+1,-4);
        $pos2 = strpos($v2,'-');
        if ($pos2==false)$index2 = -1;
        else $index2 = (int)substr($v2,$pos2+1,-4);
        return $index1-$index2;
    }
);
foreach ($files as $f){
    $rel = $v_dir.$f;
    $exec_file = $this->dir.'/'.$rel;
    $exec_file = $this->dir.'/'.$rel;
    if (!file_exists($exec_file)){
        echo "\nFile '$rel' was not found for migrations.";
        continue;
    }
    echo "\nExecute '$rel'";
    $sql = file_get_contents($exec_file);
    $this->pdo->exec($sql);
    if ($this->pdo->errorCode()!='00000'){
        echo "\nSQL Error in '$rel':\n";
        print_r($this->pdo->errorInfo());
        return;
    }
}
                                            [declaration] => public function run_migration_version($version, $up_or_down)
                                        )

                                )

                        )

                )

        )

)
