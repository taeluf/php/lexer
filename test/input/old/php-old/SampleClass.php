<?php

namespace Cats\Whatever;

/**
 * This is the best class anyone has ever written.
 */
class Sample extends cats {

    use Some\Demons;

    // First comment 
    # Second Comment

    /**
     * Why would you name a giraffe Bob?
     */
    protected $giraffe = "Bob";
    private $cat = "Jeff";
    static public $dog = "PandaBearDog";

    /**
     * dogs
     *
     * @return dogs
     */
    public function dogs($a= "abc"){
        echo "yep yep";
    }


    public const Doygle = "Hoygle Floygl";

}
