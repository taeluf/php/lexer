<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/main/Grammar.php  
  
# class Tlf\Lexer\Test\Main\Grammar  
Test features of the grammar class, not lexing, and not any language-specific grammars  
  
## Constants  
  
## Properties  
- `protected $grammar;`   
- `protected $sampleDirectiveList = [  
            'grp'=>[  
                'stop'=>[  
                                        'rewind'=>1  
                ],  
                'is'=>[  
                    ':target'=>[  
                                                'rewind'=>2,  
                    ],  
                    ':target_2'=>[],  
                ]  
            ],  
            'target'=>[  
                'stop'=>[  
                    'match'=>'abc',  
                                        'rewind'=>3,  
                ]  
            ],  
            'parent'=>[  
                'stop'=>[  
                    'then :grp'=>[  
                                                'rewind'=>4  
                    ],  
                ]  
            ],  
  
            'target_2'=>[],  
        ];` I wrote this for thinking purposes, and I will probably use it for some testing maybe????  
Might be able to delete it.  
  
## Methods   
- `public function prepare()`   
- `public function testGetDirectives()`   
- `public function testDirectiveNormalization()` Test grammar->normalizeDirective();  
- `public function testExpandIsDirective()` Test that an `is` directive expands into an array of the directives it names  
  
- `public function testDirectiveOverrides()` Rules:  
- `public function getDirectivesToLookup()`   
- `public function getSourceDirectives()`   
- `public function getOverrideDirectives()`   
- `public function getNormalizeDirectives()` Get directives as they would be defined & those same directives as they would be after normaliztion  
  
- `public function testExpandIsDirectiveWithOverrides()` Test that is directives accept overrides   
The functionality exists, i think, but the test is not implemented  
  
