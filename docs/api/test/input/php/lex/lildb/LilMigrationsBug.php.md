<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/php/lex/lildb/LilMigrationsBug.php  
  
# class Tlf\LilMigrationsBug  
This is a minimal version of LilMigrations for debugging the properties issue  
  
## Constants  
  
## Properties  
- `public $prop = 'okay';`   
  
## Methods   
- `public function ok()`   
  
