{
    "type": "file",
    "namespace": {
        "type": "namespace",
        "name": "Phad\\Test\\Integration",
        "declaration": "namespace Phad\\Test\\Integration;",
        "class": [
            {
                "type": "class",
                "docblock": {
                    "type": "docblock",
                    "description": "This class appears to test both form compilation and form submission\n",
                    "attribute": [
                        {
                            "type": "attribute",
                            "name": "notice",
                            "description": "Several of these tests incidentally test the submit target\/redirect feature."
                        }
                    ]
                },
                "namespace": "Phad\\Test\\Integration",
                "fqn": "Phad\\Test\\Integration\\Forms",
                "name": "Forms",
                "extends": "\\Phad\\Tester",
                "declaration": "class Forms extends \\Phad\\Tester",
                "properties": [
                    {
                        "type": "property",
                        "modifiers": [
                            "protected",
                            "array"
                        ],
                        "name": "blogTableColumns",
                        "value": "['id'=>'INTEGER PRIMARY KEY','title'=>'VARCHAR(200)','body'=>'VARCHAR(2000)']",
                        "declaration": "protected array $blogTableColumns = ['id'=>'INTEGER PRIMARY KEY','title'=>'VARCHAR(200)', 'body'=>'VARCHAR(2000)'];"
                    }
                ],
                "methods": [
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testDeleteItem",
                        "body": "$lildb = \\Tlf\\LilDb::sqlite();\n$pdo = $lildb->pdo();\n$phad = $this->phad();\n$phad->pdo = $pdo;\n$lildb->create('blog',['id'=>'integer', 'title'=>'varchar(90)']);\n$lildb->insert('blog',['id'=>1,'title'=>'title 1']);\n$lildb->insert('blog',['id'=>2,'title'=>'title 2']);\n$lildb->insert('blog',['id'=>3,'title'=>'title 3']);\n$form = new \\Phad\\View('Form\/Deleteable', $this->file('test\/input\/views\/'),\n    ['id'=>2, 'phad'=>$phad]);\n$form->force_compile = true;\n$form->delete();\n$blogs = $lildb->select('blog');\n$this->compare(\n    [ ['id'=>1,'title'=>'title 1'],\n      ['id'=>3,'title'=>'title 3'],\n    ],\n    $blogs\n);",
                        "declaration": "public function testDeleteItem()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testErrorMessage",
                        "body": "$pdo = $this->getPdo();\n$phad = $this->phad();\n$phad->pdo = $pdo;\n$view = $phad->view('Form\/SimpleBlogWithError');\n$_SERVER['HTTP_HOST'] = 'localhost';\n$_SERVER['HTTPS'] = 'non-empty-value';\n$BlogRow = ['body'=>'smol body', 'title'=>''];\n$out = $view->submit($BlogRow);\n$msg = $phad->validationErrorMessage($phad->failed_submit_columns);\n$this->test('Error Message Written');\n    $this->str_not_contains($out, ['<error>','<\/error>']);\n    $this->str_contains($out, '<div class=\"my-error-class\">'.$msg.'<\/div>');\n$this->test('Headers empty');\n    $this->compare(\n        [],\n        $phad->getHeaders(),\n    );",
                        "declaration": "public function testErrorMessage()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testSubmitDocumentation",
                        "body": "$_SERVER['HTTP_HOST'] = 'localhost';\n$_SERVER['HTTPS'] = 'non-empty-value';\n$ldb = \\Tlf\\LilDb::sqlite();\n$pdo = $ldb->pdo();\n$ldb->create('blog', $this->blogTableColumns);\n$phad = $this->phad();\n$phad->pdo = $pdo;\n$view = $phad->view('Form\/SimpleBlogNoTarget');\n\/\/if `id` were set, an UPDATE would be done instead.\n$BlogRow = ['title'=>'Fine Title', 'body'=>'I have to be at least 50 characters. But that isn\\'t very long.'];\n$out = $view->submit($BlogRow);\n$BlogRow['id'] = $pdo->lastInsertId();\n$headers = $phad->getHeaders();\n\/\/you can foreach(as $h){ header(...$h) }\n\/\/@export_end(Forms.ManualSubmission)\n$this->test('Headers');\n    $this->compare(\n        [['Location: https:\/\/localhost\/', true]],\n        $phad->getHeaders(),\n    );\n$this->test('Data Was Inserted');\n    $this->compare(\n        [$BlogRow],\n        $ldb->query('SELECT * FROM blog')\n    );",
                        "declaration": "public function testSubmitDocumentation()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testControllerOnSubmitDocumentation",
                        "body": "$phad = new class() extends \\Phad {\n    public function onSubmit($ItemData, &$ItemRow){\n        if ($ItemData->name!='Blog')return true;\n        $ItemRow['slug'] = str_replace(' ', '-', strtolower($ItemRow['title']));\n        return true;\n    }\n};\n$pdo = $this->getPdo();\n$phad = $this->phad($phad);\n$phad->pdo = $pdo;\n$phad->target_url = '\/blog\/{slug}\/{id}\/';\n$cols = $this->blogTableColumns;\n$cols['slug'] = 'VARCHAR(100)';\n$this->createTable($pdo, 'blog', $cols);\n$_SERVER['HTTP_HOST'] = 'localhost';\n$_SERVER['HTTPS'] = 'non-empty-value';\n$BlogRow =\n    [\n    'title'=>'Fine Title',\n    'body'=>'I have to be at least fifty characters. That shouldn\\'t be much of a problem, though it does require a bit more typing. And if I had something original to say, that would make it better.'\n    ];\n\/** anonymous class to use as our controller *\/\n\/** passing our custom controller & submitting manually *\/\n$view = $phad->view('Form\/SimpleBlogNoTarget');\n\/** $BlogRow is just an array with 'title' & 'body' **\/\n$output = $view->submit($BlogRow);\n$BlogRow['id'] = $pdo->lastInsertId();\n$BlogRow['slug'] = 'fine-title';\n$this->test('There should be no output');\n    $this->handleDidPass(strlen(trim($output))===0);\n$this->test('Headers');\n    $this->compare(\n        [['Location: https:\/\/localhost\/blog\/fine-title\/'.$BlogRow['id'].'\/', true]],\n        $phad->getHeaders(),\n    );\n$this->test('Data Was Inserted');\n    $this->compare(\n        $BlogRow,\n        $this->queryOne($pdo, 'SELECT * FROM blog'),\n    );",
                        "declaration": "public function testControllerOnSubmitDocumentation()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testWithInlineOnSubmit",
                        "body": "$pdo = $this->getPdo();\n$phad = $this->phad();\n$phad->pdo = $pdo;\n$phad->target_url = '\/blog\/{slug}\/{id}\/';\n$cols = $this->blogTableColumns;\n$cols['slug'] = 'VARCHAR(100)';\n$this->createTable($pdo, 'blog', $cols);\n$_SERVER['HTTP_HOST'] = 'localhost';\n$_SERVER['HTTPS'] = 'non-empty-value';\n$BlogRow =\n    [\n    'title'=>'Fine Title',\n    'body'=>'I have to be at least fifty characters. That shouldn\\'t be much of a problem, though it does require a bit more typing. And if I had something original to say, that would make it better.'\n    ];\n$view = $phad->view('Form\/BlogWithOnSubmit');\n$output = $view->submit($BlogRow);\n$BlogRow['id'] = $pdo->lastInsertId();\n$BlogRow['slug'] = 'fine-title';\n$this->test('Backend Prop is removed');\n    $this->str_not_contains($phad->view('Form\/BlogWithOnSubmit', $BlogRow), 'type=\"backend\"');\n$this->test('<onsubmit> tag was removed');\n    $this->str_not_contains($phad->view('Form\/BlogWithOnSubmit', $BlogRow), '<onsubmit>');\n$this->test('There should be no output');\n    $this->handleDidPass(strlen(trim($output))===0);\n$this->test('Headers');\n    $this->compare(\n        [['Location: https:\/\/localhost\/blog\/fine-title\/'.$BlogRow['id'].'\/', true]],\n        $phad->getHeaders(),\n    );\n$this->test('Data Was Inserted');\n    $this->compare(\n        $BlogRow,\n        $this->queryOne($pdo, 'SELECT * FROM blog'),\n    );",
                        "declaration": "public function testWithInlineOnSubmit()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testInsertWithValidOption",
                        "body": "$pdo = $this->getPdo();\n$phad = $this->phad();\n$phad->pdo = $pdo;\n$view = $phad->view('Form\/BlogWithCategories');\n$cols = $this->blogTableColumns;\n$cols['category'] = 'VARCHAR(100)';\n$this->createTable($pdo, 'blog', $cols);\n$_SERVER['HTTP_HOST'] = 'localhost';\n$_SERVER['HTTPS'] = 'non-empty-value';\n$BlogRow = ['title'=>'Police Brutality super sucks', 'body'=>'Its been well documented that police brutality is a reality, yet there\\'s still overwhelming controversy about what we should do with our society. Some say: Let the police to what they gotta do. Others say: Lets rethink this society that\\'s built upon punishing people who misbehave & actually help people instead. Mayb ewe could redirect some of the MASSIVE tax dollars that go to police departments to actually help someone. That\\'s me. I say that. - Reed',\n    'category'=>'Police Brutality'];\n$out = $view->submit($BlogRow);\n$BlogRow['id'] = $pdo->lastInsertId();\n$this->test('There should be no output');\n    $this->handleDidPass(strlen(trim($out))===0);\n$this->test('Headers');\n    $this->compare(\n        [['Location: https:\/\/localhost\/blog\/'.$BlogRow['title'].'\/', true]],\n        $phad->getHeaders(),\n    );\n$this->test('Data Was Inserted');\n    $this->compare(\n        $BlogRow,\n        $this->queryOne($pdo, 'SELECT * FROM blog'),\n    );",
                        "declaration": "public function testInsertWithValidOption()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testUpdateRedirectsToTarget",
                        "body": "\/\/ $phad = $this->phad();\n$pdo = $this->getPdo();\n\/\/ $phad->pdo = $pdo;\n$this->createTable($pdo, 'blog', $this->blogTableColumns);\n$_SERVER['HTTP_HOST'] = 'localhost';\n$_SERVER['HTTPS'] = 'non-empty-value';\n$phad = new class() extends \\Phad {\n    public function t(){\n        echo \"\\ntttttttttttttttt\\n\";\n    }\n    public function objectFromRow($ItemData, $BlogRow){\n        $Blog = (object)$BlogRow;\n        $Blog->slug = str_replace(' ', '-', strtolower($Blog->title));\n        return $Blog;\n    }\n};\n$phad = $this->phad($phad);\n$phad->target_url = '\/blog\/{slug}\/{id}\/';\n$phad->pdo = $pdo;\n$view = $phad->view('Form\/SimpleBlogNoTarget', ['phad'=>$phad]);\n$BlogRow =\n    [\n    'title'=>'Fine Title',\n    'body'=>'I have to be at least fifty characters. That shouldn\\'t be much of a problem, though it does require a bit more typing. And if I had something original to say, that would make it better.'\n    ];\n$out = $view->submit($BlogRow);\n$BlogRow['id'] = $pdo->lastInsertId();\n$this->test('There should be no output');\n    $this->handleDidPass(strlen(trim($out))===0);\n$this->test('Headers');\n    $this->compare(\n        [['Location: https:\/\/localhost\/blog\/fine-title\/'.$BlogRow['id'].'\/', true]],\n        $phad->getHeaders(),\n    );\n$this->test('Data Was Inserted');\n    $this->compare(\n        $BlogRow,\n        $this->queryOne($pdo, 'SELECT * FROM blog'),\n    );",
                        "declaration": "public function testUpdateRedirectsToTarget()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testUpdateValid",
                        "body": "$_SERVER['HTTP_HOST'] = 'localhost';\n$_SERVER['HTTPS'] = 'non-empty-value';\n$ldb = \\Tlf\\LilDb::sqlite();\n$phad = $this->phad();\n$phad->pdo = $ldb->pdo();\n$view = $phad->view('Form\/SimpleBlog');\n$ldb->create('blog', $this->blogTableColumns);\n$BlogRow = ['id'=>0, 'title'=>'Fine Title', 'body'=>'I have to be at least fifty characters. That shouldn\\'t be much of a problem, though it does require a bit more typing. And if I had something original to say, that would make it better.'];\n$ldb->insert('blog', $BlogRow);\n$UpdatedBlog = $BlogRow;\n$UpdatedBlog['title'] = 'New Title';\n$out = $view->submit($UpdatedBlog);\n$this->test('Headers');\n    $this->compare(\n        [['Location: https:\/\/localhost\/blog\/'.$UpdatedBlog['title'].'\/', true]],\n        $phad->getHeaders(),\n    );\n$this->test('Data Was Updated');\n    $this->compare(\n        [$UpdatedBlog],\n        $ldb->query('SELECT * FROM blog')\n    );\n$this->test('No Output Present');\n    $this->compare(\n        '',\n        trim($out)\n    );",
                        "declaration": "public function testUpdateValid()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testInsertValid",
                        "body": "$phad = $this->phad();\n$phad->pdo = $this->getPdo();\n$view = $phad->view('Form\/SimpleBlog');\n$this->createTable($phad->pdo, 'blog', $this->blogTableColumns);\n$_SERVER['HTTP_HOST'] = 'localhost';\n$_SERVER['HTTPS'] = 'non-empty-value';\n$BlogRow = ['title'=>'Fine Title', 'body'=>'I have to be at least fifty characters. That shouldn\\'t be much of a problem, though it does require a bit more typing. And if I had something original to say, that would make it better.'];\n$out = $view->submit($BlogRow);\n$BlogRow['id'] = $phad->pdo->lastInsertId();\n$this->test('There should be no output');\n    $this->handleDidPass(strlen(trim($out))===0);\n$this->test('Headers');\n    $this->compare(\n        [['Location: https:\/\/localhost\/blog\/'.$BlogRow['title'].'\/', true]],\n        $phad->getHeaders(),\n    );\n$this->test('Data Was Inserted');\n    $this->compare(\n        $BlogRow,\n        $this->queryOne($phad->pdo, 'SELECT * FROM blog'),\n    );",
                        "declaration": "public function testInsertValid()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "docblock": {
                            "type": "docblock",
                            "description": "",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "test",
                                    "description": "that a partially filled in form is returned when submission fails."
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "testSubmitInvalid",
                        "body": "$phad = $this->phad();\n$phad->force_compile = false;\n$this->createTable($phad->pdo, 'blog', $this->blogTableColumns);\n\/\/ View contains:\n\/\/ <textarea name=\"body\" maxlength=\"2000\" minlength=\"50\"><\/textarea>\n$view = $phad->view('Form\/SimpleBlog');\n\/\/ should fail because body is less than 50 chars\n$Blog = ['title'=>'Fine Title', 'body'=>'body too short'];\n$out = $view->submit($Blog);\n$Blog = (object)$Blog;\necho $out;\n$this->test('Form cleaned up');\n    $this->str_not_contains($view, ['item=']);\n$this->test('Submitted Blog Content');\n    $this->str_contains($out, 'value=\"'.$Blog->title.'\"');\n    $this->str_contains($out, '>'.$Blog->body.'<\/textarea>');",
                        "declaration": "public function testSubmitInvalid()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "testDisplayWithNoObject",
                        "body": "$view = $this->view('Form\/SimpleBlog');\n$Blog = ['title'=>'Fine Title', 'body'=>'body too short'];\n$out = $view->outputWithEmptyObject($Blog);\n$Blog = (object)$Blog;\necho $out;\n$this->test('Form cleaned up');\n    $this->str_not_contains($out, ['item=']);\n$this->test('Target Attribute Removed');\n    $this->str_not_contains($out, 'target=\"');\n$this->test('Submitted Blog Content');\n    $this->str_contains($out, 'value=\"\"');\n    $this->str_contains($out, '><\/textarea>');",
                        "declaration": "public function testDisplayWithNoObject()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "docblock": {
                            "type": "docblock",
                            "description": "",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "test",
                                    "description": "getting each selectable-option from a form's compiled view"
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "testHasSelectOptions",
                        "body": "$view = $this->view('Form\/BlogWithCategories');\n$ItemData = $view->getItemData();\n$expect = [\n    'title' =>\n    [\n        'type' => 'text',\n        'maxlength' => '75',\n        'tagName' => 'input',\n    ],\n    'body' =>\n    [\n        'maxlength' => '2000',\n        'minlength' => '50',\n        'tagName' => 'textarea',\n    ],\n    'category'=>\n    [\n        'tagName'=> 'select',\n        'options'=>[\n            \"social-justice\",\n            \"policy\",\n            \"Police Brutality\",\n            \"Election\",\n        ],\n    ],\n    'id'=>\n    [\n        'tagName'=>'input',\n        'type'=>'hidden',\n    ]\n];\n$this->compare($expect, $ItemData->properties);",
                        "declaration": "public function testHasSelectOptions()"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "docblock": {
                            "type": "docblock",
                            "description": "",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "test",
                                    "description": "getting info about properties from the compiled view."
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "testHasPropertiesData",
                        "body": "$view = $this->view('Form\/SimpleBlog');\n$ItemData = $view->getItemData();\n$expect = [\n    'title' =>\n    [\n        'type' => 'text',\n        'maxlength' => '75',\n        'tagName' => 'input',\n    ],\n    'body' =>\n    [\n        'maxlength' => '2000',\n        'minlength' => '50',\n        'tagName' => 'textarea',\n    ],\n    'id'=>\n    [\n        'tagName'=>'input',\n        'type'=>'hidden',\n    ]\n];\n$this->compare($expect, $ItemData->properties);",
                        "declaration": "public function testHasPropertiesData()"
                    }
                ]
            }
        ]
    }
}