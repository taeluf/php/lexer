<?php

/**
 * A class that does nothing
 */
class DocumentationExample extends \Nothing {

    /** 
     * A method that does nothing.
     */
    public function one(){}
    public function two(){}
}
