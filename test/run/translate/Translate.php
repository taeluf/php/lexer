<?php

namespace Tlf\Lexer\Test\Translate;

/**
 * Tests output ASTs as code
 */
class Translate extends \Tlf\Tester {

    public function testOutputSampleClass(){
        $this->disable();

        $path = $this->file('test/output/php/tree/SampleClass.js');
        $data = json_decode(file_get_contents($path), true);
        $class_data = $data['namespace']['class'][0];
        var_dump($class_data);
        $ast = new \Tlf\Lexer\Ast\ClassAst('class', $class_data);


        $php = $ast->getCode('php');

        echo "\n\n\n\n------###### PHP #####\n$php\n--------------------\n\n\n\n";

        $js = $ast->getCode('javascript');

        echo "\n\n\n\n------###### JAVASCRIPT #####\n$js\n--------------------\n\n\n\n";

        //exit;

    }


    public function testOutputPhpAndJs(){

        $this->disable();


        $property_source = 'protected $giraffe = "Bob";';
        $property_ast = [
            'type'=>'property',
            'name'=>'giraffe',
            'modifiers' => ['protected'],
            'docblock'=> [
                'type'=>'docblock',
                'description' => 'Why would you name a giraffe Bob?',
            ],

            'value'=> "Bob",
        ];

        $class_ast = [
            'type'=>'class',
            'fqn'=> 'Sample',
            'namespace' => '',
            'name'=>'Sample',
            'extends' => 'cats',
            'methods' => [],
            'properties'=> [$property_ast],
            'const'=>[],
        ];

        $ast = new \Tlf\Lexer\Ast\ClassAst('class',$class_ast);

        $php = $ast->getCode('php');

        echo "\n\n\n\n------###### PHP #####\n$php\n--------------------\n\n\n\n";

        $js = $ast->getCode('javascript');

        echo "\n\n\n\n------###### JAVASCRIPT #####\n$js\n--------------------\n\n\n\n";

        //exit;

    }
}
