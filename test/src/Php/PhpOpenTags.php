<?php

namespace Tlf\Lexer\Test\Directives;

trait PhpOpenTags {
    protected $_php_open_tags_tests = [

        'Open.Class'=>[
            'ast.type'=>'file',
            'start'=>['php_open'],
            'input'=>'<html><div><?php class Abc {} ',
            'expect'=>[
                'namespace'=>'',
                'class'=>[
                    0=>[
                        'type'=>'class',
                        'fqn'=>'Abc',
                        'namespace'=>'',
                        'name'=>'Abc',
                        'declaration'=>'class Abc',
                    ]
                ],
            ],
        ],
        'Open.Close.Open'=>[
            'ast.type'=>'file',
            'start'=>['php_open'],
            'input'=>'<html><div><?php ?></div><?php namespace Abc; ',
            'expect'=>[
                'namespace'=>[
                    'type'=>'namespace',
                    'name'=>'Abc',
                    'declaration'=>'namespace Abc;',
                ],
            ],
        ],
        'Open.Simple'=>[
            'ast.type'=>'file',
            'start'=>['php_open'],
            'input'=>'<html><div><?php namespace Abc;',
            'expect'=>[
                'namespace'=>[
                    'type'=>'namespace',
                    'name'=>'Abc',
                    'declaration'=>'namespace Abc;',
                ],
            ],
        ],
    ];
}
