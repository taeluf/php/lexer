<?php

namespace Taeluf\PHTML;

class TextNode extends \DOMText {


    /** is this node the given tag
    */
    public function is(string $tagName): bool{
        if (strtolower($this->nodeName)==strtolower($tagName))return true;
        return false;
    }

}
