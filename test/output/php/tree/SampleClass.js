{
    "type": "file",
    "namespace": {
        "type": "namespace",
        "name": "Cats\\Whatever",
        "declaration": "namespace Cats\\Whatever;",
        "class": [
            {
                "type": "class",
                "docblock": {
                    "type": "docblock",
                    "description": "This is the best class anyone has ever written."
                },
                "namespace": "Cats\\Whatever",
                "fqn": "Cats\\Whatever\\Sample",
                "name": "Sample",
                "extends": "cats",
                "declaration": "class Sample extends cats",
                "comments": [
                    "First comment",
                    "Second Comment"
                ],
                "properties": [
                    {
                        "type": "property",
                        "modifiers": [
                            "protected"
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "Why would you name a giraffe Bob?"
                        },
                        "name": "giraffe",
                        "value": "\"Bob\"",
                        "declaration": "protected $giraffe = \"Bob\";"
                    },
                    {
                        "type": "property",
                        "modifiers": [
                            "private"
                        ],
                        "name": "cat",
                        "value": "\"Jeff\"",
                        "declaration": "private $cat = \"Jeff\";"
                    },
                    {
                        "type": "property",
                        "modifiers": [
                            "static",
                            "public"
                        ],
                        "name": "dog",
                        "value": "\"PandaBearDog\"",
                        "declaration": "static public $dog = \"PandaBearDog\";"
                    }
                ],
                "methods": [
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "name": "a",
                                "value": "\"abc\"",
                                "declaration": "$a= \"abc\""
                            }
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "dogs\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "dogs"
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "dogs",
                        "body": "echo \"yep yep\";",
                        "declaration": "public function dogs($a= \"abc\")"
                    }
                ],
                "const": [
                    {
                        "type": "const",
                        "name": "Doygle",
                        "modifiers": [
                            "public"
                        ],
                        "value": "\"Hoygle Floygl\"",
                        "declaration": "public const Doygle = \"Hoygle Floygl\";"
                    }
                ]
            }
        ]
    }
}