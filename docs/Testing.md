<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Grammar Testing  
Unit testing is the best way I've found to really develop a grammar. It allows you to focus on very specific pieces of the grammar at a time, then later put it all together by parsing an entire document.  
  
This library depends upon [php-tests](https://tluf.me/php-tests) for testing.  
  
Links:  
- Writing a Grammar: [docs/GrammarWriting.md](/docs/GrammarWriting.md)  
- Grammar Example: [docs/GrammarExample.md](/docs/GrammarExample.md)  
- Grammar Commands: [docs/GrammarCommands.md](/docs/GrammarCommands.md)  
  
## Howto  
1. Extend `\Tlf\Lexer\Test\Tester` which extends from `\Tlf\Tester` or copy the methods from it & modify as needed for a different testing library.  
2. Implement a `testGrammarName()` method that uses the test runner  
3. Implement the data structure for defining the unit tests.  
  
Example of `2.`:  
```php  
Template 'methods.testPhpDirectives.definition' does not exist. {  
Template 'methods.testPhpDirectives.body' does not exist.  
}  
```  
  
Example of  `3.`:  
-- whatever goes here  
  
