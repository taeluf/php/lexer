<?php

namespace Tlf\Lexer\PhpNew;

trait CoreDirectives {

    public $_core_directives = [
        'php_open'=>[
            // I could support short open, but I'm not going to right now
            'start'=>[
                'match'=>'<?php', 
                'buffer.clear',
                'then :php_code',
            ],
            'stop'=>[
                'match' => '?>',
                'buffer.clear',
            ]
        ],
        'php_code'=>[
            'start'=>[
                'rewind 1',
                'then :word',
                'then :operation',
                'then :whitespace',
                'then :comment',
                'then docblock:/*',
                'then :string',
                'then :+php_stop'=>[
                    'start'=>[
                        'match'=>'?>',
                        'directive.pop 2',
                        'rewind 2',
                        'buffer.clear',
                    ]
                ],
            ],
            'stop'=>[
                'debug.die'=>'php_code.stop should never execute, because the stopping is handled by the \'start\' of another directive',
                // 'match'=>'? >',
                // 'directive.pop 1',
                // 'rewind 2',
                // 'buffer.clear',
            ]
        ],

        'operation'=>[
            'start'=>[
                'this:operation_match',
                'match'=>'/./', // easiest way to do the propert starting & everything
                'this:handleOperation',
                'buffer.clear',
                'stop',
            ]
        ],
        'word'=>[
            'start'=>[
                // 'match'=>'/([a-zA-Z0-9\_\\\\])[^a-zA-Z0-9\_\\\\]$/',
                'match'=>'/^[a-zA-Z0-9\_\\\\]$/',
            ],
            'stop'=>[
                'match'=>'/[^a-zA-Z0-9\_\\\\]$/',
                'rewind 1',
                'this:handleWord',
                'buffer.clear',
            ],
        ],

        'whitespace'=>[
            //is 'i' dotall? & does that make it catch newlines?
            'start'=>[
                'match' =>'/^\s$/i',
                'lexer:unsetPrevious whitespace',
            ],
            'stop'=>[
                'match'=> '/[^\s]$/i',
                'rewind 1',
                'this:handleWhitespace',
                'buffer.clear',
            ],
        ],

        'comment'=>[
            'start'=>[
                'match'=> '/(\\/\\/|\#)/',
                'buffer.clear'=>true,
            ],

            'stop'=>[
                'match'=>'/(\r\n|\n)$/',
                'this:handleComment',
                'buffer.clear',
            ]
        ],
    ];
}
