<?php

function abc(){
}

class Abc {
}

?>

# File <?=$File->relPath?>  

## Functions
<?php
foreach ($File->function??[] as $function){
    $function = (object)$function;
    $descript = $function->docblock['tip'] ?? $function->docblock['description'] ?? '';
    $name = $function->name;
    echo "- `$name`: $descript\n";
}

return;
?>

## Properties
<?php
foreach ($Class->property as $prop){
    $prop = (object)$prop;
    $def = $prop->definition;
    $descript = $prop->docblock;
    echo "- `${def}` ${descript}\n";
}

?>

## Methods 
<?php
foreach ($Class->method as $method){
    $method = (object)$method;
    $def = $method->declaration;
    // $descript = $method->description;
    $descript = $method->docblock;
    echo "- `${def}` ${descript}\n";
}

?>

## Static Properties 
<?php
foreach ($Class->staticProps as $prop){
    $def = $prop->definition;
    $descript = $prop->description;
    echo "- `${def}` ${descript}\n";
}

?>

## Static functions
<?php
foreach ($Class->staticFunctions as $func){
    $def = $func->definition;
    $descript = $func->description;
    echo "- `${def}` ${descript}\n";
}

?>

<?php

print_r($Class);

class Def {
}

function yep(){
}

?>
