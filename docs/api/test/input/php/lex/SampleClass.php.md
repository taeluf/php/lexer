<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/php/lex/SampleClass.php  
  
# class Cats\Whatever\Sample  
This is the best class anyone has ever written.  
  
## Constants  
- `public const Doygle = "Hoygle Floygl";`   
  
## Properties  
- `protected $giraffe = "Bob";` Why would you name a giraffe Bob?  
- `private $cat = "Jeff";`   
- `static public $dog = "PandaBearDog";`   
  
## Methods   
- `public function dogs($a= "abc")` dogs  
  
  
