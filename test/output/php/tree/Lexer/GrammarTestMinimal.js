{
    "type": "file",
    "namespace": {
        "type": "namespace",
        "name": "Tlf\\Lexer\\Test\\Main",
        "declaration": "namespace Tlf\\Lexer\\Test\\Main;",
        "class": [
            {
                "type": "class",
                "docblock": {
                    "type": "docblock",
                    "description": "Test features of the grammar class, not lexing, and not any language-specific grammars"
                },
                "namespace": "Tlf\\Lexer\\Test\\Main",
                "fqn": "Tlf\\Lexer\\Test\\Main\\Grammar",
                "name": "Grammar",
                "extends": "\\Tlf\\Tester",
                "declaration": "class Grammar extends \\Tlf\\Tester",
                "methods": [
                    {
                        "type": "method",
                        "args": [],
                        "docblock": {
                            "type": "docblock",
                            "description": "Get directives as they would be defined & those same directives as they would be after normaliztion\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "test",
                                    "description": ""
                                },
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "multi dimensional array"
                                },
                                {
                                    "type": "attribute",
                                    "name": "child",
                                    "description": ".index1 is the input directive, as one would define it"
                                },
                                {
                                    "type": "attribute",
                                    "name": "child",
                                    "description": ".index2 is the expected output directive, as returned from the normalize method"
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "getNormalizeDirectives",
                        "body": "",
                        "declaration": "public function getNormalizeDirectives()"
                    }
                ]
            }
        ]
    }
}