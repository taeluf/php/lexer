<?php

namespace Tlf\Lexer\Test;

/**
 * These are tests that either aren't really tests or that probably don't have a place any more
 */
class DefunctTests extends \Tlf\Lexer\Test\Tester {

    /** convert a tree into json to see if i like the output better 
     *
     * This was never a test. Just a way to run some code.
     */
    public function testMakeJson(){
        $this->disable();
        return;
        $file = $this->file('test/input/php/');
        $tree = require($file.'tree/SampleClass.tree.php');
        $json = json_encode($tree, JSON_PRETTY_PRINT);
        file_put_contents($file.'tree/SampleClass.tree.json', $json);

    }
    /**
     * An old test I was using to design the new php grammar.
     * It is just a mess now and i don't want to mess with it.
     */
    public function testPhpGrammarNew_SampleClass(){

        echo "it's wrong. I don't know why, and I don't want to fix it.";
        $this->disable();

        return;


        // $dir = dirname(__DIR__).'/php-new/';
        // $dir = dirname()
        $dir = $this->file('test/input/php/lex/');
        $file = $dir.'SampleClass.php';
        $targetTree = file_get_contents($this->file('test/input/php/tree/').'SampleClass.js');
        $targetTree = json_decode($targetTree, true);

        $lexer = new \Tlf\Lexer();
        $lexer->debug = true;

        //the string rewrite
        // $lexer->stop_loop = 305; // on loop 293, we start listening for strings. string_double STOPS on loop 299
        //on loop 300, 

        // This is for devving v0.6:
        // $lexer->stop_loop = 18;
        // $lexer->stop_loop = 33;
        // $lexer->stop_loop = 21; 
        // $lexer->stop_loop = 31;
        // $lexer->stop_loop = 41;
        // $lexer->stop_loop = 51;
        // $lexer->stop_loop = 111; // catching 'class'
        // $lexer->stop_loop = 120;
        // $lexer->stop_loop = 135; // block starts at 132
        // $lexer->stop_loop = 150; // use trait is about 150
        // $lexer->stop_loop = 185; // 181 is new line for first comment
        // $lexer->stop_loop = 206; // process # second comment at 203
        // $lexer->stop_loop = 261; // Process first property's docblock
        // $lexer->stop_loop = 280; // 277 'modifier' starts
        // $lexer->stop_loop = 300; // first property processed on 299
        // $lexer->stop_loop = 335; // second property is complete
        // $lexer->stop_loop = 395; // static public property is finished by loop 385
        // $lexer->stop_loop = 445; // method docblock finishes on 441
        // $lexer->stop_loop = 460; // `public ` is captured on 454, starting method modifier
        // $lexer->stop_loop = 492; // method block (opening curly brace) starts on 473
        // $lexer->stop_loop = 525; // closing method curly brace is loop 516
        // $lexer->stop_loop = 553; // issues around `=` around 550
        // $lexer->stop_loop = 585; // see 542 for `const `. `=` is loop 551. string closes on 578
        // $lexer->stop_loop = 800;
        // $lexer->stop_loop = 1200;

        
        // $lexer->stop_loop = 9999999;
        $lexer->useCache = false; // useCache only matters when lexing a file
        $lexer->addGrammar($phpGrammar = new \Tlf\Lexer\PhpGrammarNew());

        $ast = $lexer->lexFile($file);

        $tree = $ast->getTree(); // not what we normally want with json

        echo "\n\n\n --- file AST ---\n\n";

        print_r($tree);

        //
        // write the current tree to a file
        //
        file_put_contents($dir.'SampleClass.tree2.php',var_export($tree,true));
        echo "\n----\n\n\n";
        $this->compare(
            $targetTree, 
            $tree
        );
    }
}
