<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Grammar.php  
  
# class Tlf\Lexer\Grammar  
  
See source code at [/src/Grammar.php](/src/Grammar.php)  
  
## Constants  
  
## Properties  
- `protected $blankCount = 0;`   
- `public $directives = [];` The actual array of directives, built during onGrammarAdded()  
- `protected \Tlf\Lexer $lexer;` the lexer currently running  
  
## Methods   
- `public function lexer_started($lexer, $ast, $token)`   
- `public function setLexer($lexer)`   
- `public function getNamespace()` Get a namespace prefix to use for specifying what directive to target  
  
