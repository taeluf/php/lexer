<?php

namespace Tlf\Lexer\Test;


class BashGrammar extends \Taeluf\Tester {

    public function testABash5(){
        echo "\nThere is no passing condition for this test. It just prints.\n\n";
        $file = dirname(__DIR__).'/extra/Sample5.bash';
        $lexer = new \Tlf\Lexer();
        $lexer->addGrammar(new \Tlf\Lexer\BashGrammar());
        $ast = $lexer->lex($file);
        unset($ast->source);
        $tree = $ast->getTree();
        print_r($tree);
        // exit;
        //
        return false;
    }

    public function testABash4(){
        echo "\nThere is no passing condition for this test. It just prints.\n\n";
        $file = dirname(__DIR__).'/extra/Sample4.bash';
        $lexer = new \Tlf\Lexer();
        $lexer->addGrammar(new \Tlf\Lexer\BashGrammar());
        $ast = $lexer->lex($file);
        unset($ast->source);
        $tree = $ast->getTree();
        print_r($tree);
        // exit;
        //
        return false;
    }

    public function testABash3(){
        echo "\nThere is no passing condition for this test. It just prints.\n\n";
        $file = dirname(__DIR__).'/extra/Sample3.bash';
        $lexer = new \Tlf\Lexer();
        $lexer->addGrammar(new \Tlf\Lexer\BashGrammar());
        $ast = $lexer->lex($file);
        unset($ast->source);
        $tree = $ast->getTree();
        print_r($tree);
        // exit;
        //
        return false;
    }

    public function testABash2(){
        echo "\nThere is no passing condition for this test. It just prints.\n\n";
        $file = dirname(__DIR__).'/extra/Sample2.bash';
        $lexer = new \Tlf\Lexer();
        $lexer->addGrammar(new \Tlf\Lexer\BashGrammar());
        $ast = $lexer->lex($file);
        unset($ast->source);
        $tree = $ast->getTree();
        print_r($tree);
        // exit;
        //
        return false;
    }

    public function testABash(){
        echo "\nThere is no passing condition for this test. It just prints.\n\n";
        $file = dirname(__DIR__).'/extra/Sample.bash';
        $lexer = new \Tlf\Lexer();
        $lexer->addGrammar(new \Tlf\Lexer\BashGrammar());
        $ast = $lexer->lex($file);
        unset($ast->source);
        $tree = $ast->getTree();
        print_r($tree);
        // exit;
        //
        return false;
    }

}
