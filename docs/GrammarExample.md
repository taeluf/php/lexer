<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Example  
This is a really simple example of a functional, tested grammar. They get much more complex.  
- Get further grammar-writing instructions in [docs/GrammarWriting.md](/docs/GrammarWriting.md)  
- Look at the commands available in [docs/GrammarCommands.md](/docs/GrammarCommands.md)  
  
  
## StarterGrammar's Directives  
Most grammars will have more directives & multiple traits to facilitate organization. This example only uses one trait.  
```php  
'/home/reed/data/owner/Reed/projects/php/Lexer/code/Starter/OtherDirectives.php' is not a file.  
```  
  
## StarterGrammar  
See that directives are built during `onGrammarAdded()` from the traits.  
```php  
'/home/reed/data/owner/Reed/projects/php/Lexer/code/Starter/StarterGrammar.php' is not a file.  
```  
  
