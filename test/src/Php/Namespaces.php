<?php

namespace Tlf\Lexer\Test\Directives;

trait Namespaces {
    protected $_namespace_tests = [
        'Namespace.Docblock'=>[
            'start'=>['php_code'],
            'input'=>"/** docs */ namespace AbcDef_09;",
            'expect'=>[
                'namespace'=>[
                    'type'=>'namespace',
                    'docblock'=>[
                        'type'=>'docblock',
                        'description'=>'docs',
                    ],
                    'declaration'=>'namespace AbcDef_09;',
                    'name'=>'AbcDef_09',
                ],
            ],
        ],

        'Namespace.MultiLayer.IntermediateDocblocks'=>[
            'is_bad_test'=>'The declaration SHOULD have a space after `namespace`, but it doesn\'t. I don\'t plan to fix this. The test is passing because this bug is acceptable. ',
            'start'=>['php_code'],
            'input'=>"namespace/*k*/Abc\Def\_09;",
            'expect'=>[
                'namespace'=>[
                    'type'=>'namespace',
                    'declaration'=>'namespaceAbc\Def\_09;',
                    'name'=>'Abc\Def\_09',
                ],
            ],
        ],

        'Namespace.MultiLayer'=>[
            'start'=>['php_code'],
            'input'=>"namespace Abc\Def\_09;",
            'expect'=>[
                'namespace'=>[
                    'type'=>'namespace',
                    'declaration'=>'namespace Abc\Def\_09;',
                    'name'=>'Abc\Def\_09',
                ],
            ],
        ],

        'Namespace.Simple'=>[
            'start'=>['php_code'],
            'input'=>"namespace AbcDef_09;",
            'expect'=>[
                'namespace'=>[
                    'type'=>'namespace',
                    'declaration'=>'namespace AbcDef_09;',
                    'name'=>'AbcDef_09',
                ],
            ],
        ],
    ];
}
