<?php

namespace Tlf\Lexer\Php;

trait OtherDirectives {

    protected $_other_directives = [
        // 'html'=>[
        //     //this whole array essentially becomes the regexes array from the Html grammar. Except whats declared here overrides what's declared there.
        //     'grammar'=>'Html',
        //     //to automatically setup the html grammar to run this...
        //     //But then how does the html grammar know when it needs to stop?
        //
        //
        //     // This 'directives'=> ... will be added the root of $htmlGrammar->regexes.
        //     // Maybe as _directives to indicate special function
        //     // These will be checked every single time a char is added to the html
        //     // every single time...
        //     '_directives'=>[':php_open','php_echo'],
        // ],

        // @todo I need to apply php_close to almost everything...

        'whitespace'=>[
            //is 'i' dotall? & does that make it catch newlines?
            'start'=>[
                'match' =>'/(\s+)$/i',
                'lexer:unsetPrevious whitespace',
            ],
            'stop'=>[
                'match'=> '/[^\s]$/i',
                'rewind 1 //whitespace-stop',
                'previous.set whitespace',
                'buffer.clear',
            ],
        ],

        // 'docblock'=>[
            // 'start'=>[
                // 'match'=>'/(\/\*\*?)/',
                // 'then :docstar',
            // ],
            // 'stop'=> [
                // 'match'=>'/\\*\\//',
                // 'this:processDocBlock',
                // 'buffer.clear',
            // ],
        // ],

        'comment'=>[
            'start'=>[
                'match'=> '/(\\/\\/|\#)/',
                'buffer.clear'=>true,
            ],

            'stop'=>[
                'match'=>'/(\r\n|\n)$/',
                'this:processComment',
                'buffer.clear',
            ]
        ],

        'docstar'=>[
            'start'=>[
                'match'=>'/\*/',
                'stop',
                'directive.pop 1'
            ],
        ],

    ];
}
