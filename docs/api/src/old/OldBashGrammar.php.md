<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/old/OldBashGrammar.php  
  
# class Tlf\Lexer\OldBashGrammar  
  
See source code at [/src/old/OldBashGrammar.php](/src/old/OldBashGrammar.php)  
  
## Constants  
  
## Properties  
- `protected $regexes = [  
  
        'docblockStart'=>[  
            'regex'=>'/(##.*)/',  
            'state'=>[null, 'comment'],  
        ],  
        'docblockEnd'=>[  
            'regex'=>'/(^\s*[^\#])/m',  
            'state'=>['docblock'],  
        ],  
        'function'=>[  
            'regex'=>'/(?:function\s+)?([a-zA-Z\_0-9]*)(?:(?:\s*\(\))|\s+)\{/',  
            'state'=>[null],  
        ],  
        'comment'=>[  
            'regex'=> '/#[^#]/',  
            'state'=>[null],  
        ],  
        'commentEnd'=>[  
            'regex'=> '/#[^\n]*\n/m',  
            'state'=>['comment'],  
        ]  
    ];`   
  
## Methods   
- `public function onLexerStart($lexer,$file,$token)`   
- `public function onComment($lexer, $fileAst, $token)`   
- `public function onCommentEnd($lexer, $fileAst, $token)`   
- `public function onDocblockStart($lexer, $ast, $token)`   
- `public function onDocblockEnd($lexer, $ast, $token)`   
- `public function onFunction($lexer, $file, $token)`   
  
