#!/usr/bin/env bash

function help(){

    sub="$1"
    if [[ -n "$sub" ]];then
        import "core/$sub"
        if is_function "${sub}_help" ;then
            msg_header "[bent help $sub]"
            run "$sub" "help"
            return;
        else
            msg_notice "No help menu for '${sub}'"
        fi
    fi

    msg_instruct "bent [command_group] [command]${cOff} - for 'core' group, just 'bent [command]'"
    msg_instruct "bent [command_group]${cOff} for help or ${cInstruct}bent [command_group] help${cOff}"

    prompt_choose_function \
        "# bent help [command]" \
            "run help jargon" "jargon" "Learn the jargon" \
            "run help cli" "cli" "Learn some cli basics" \
            \
        "# bent [command_group]" \
            "run help core" "core" "Core commands like save & upload. bent [command] instead of bent core [command]" \
            "run help new" "new" "Create a new project, version, or feature" \
            "run help switch" "switch" "Switch between versions, projects, or features" \
            "run help show" "show" "Show stuff" \
            "run help log" "log" "View commit history" \
            "run help delete" "delete" "Delete a project version" \
            "run help setup" "setup" "Setup Stuff" \
            "run help move" "move" "Move a project to a new host" \
            "run help ssh" "ssh" "Work with ssh keys" \
    ;
}

function help_jargon(){
    msg_header "This help under development"

    msg_ulist  \
        "git: A type of version control, that keeps track of changes to your code. Two others are mercurial and svn." \
        "repo, repository: A project directory that is stored in version control" \
        "branch: A version of your project." \
        "There's more, but that's all I'm doing for now" \
    ;
}

function help_cli(){
    msg_notice "General cli help has not yet been written"
    # @TODO write general cli help
}

function help_bent(){
    msg_notice "General help info about Git Bent has not been written yet"
    # @TODO write general help about Git Bent, like using q to quit prompts & how commands work & such. Maybe it could include contact info?
}
