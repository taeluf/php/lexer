<?php
return 
array (
  'type' => 'file',
  'ext' => 'php',
  'name' => 'DocumentationExample',
  'path' => '/home/reed/data/projects/php/Lexer/test/input/php/DocumentationExample.php',
  'namespace' => '',
  'class' => 
  array (
    0 => 
    array (
      'type' => 'class',
      'docblock' => 
      array (
        'type' => 'docblock',
        'description' => 'A class that does nothing',
      ),
      'fqn' => 'DocumentationExample',
      'namespace' => '',
      'name' => 'DocumentationExample',
      'extends' => '\\Nothing',
      'declaration' => 'class DocumentationExample extends \\Nothing',
      'methods' => 
      array (
        0 => 
        array (
          'type' => 'method',
          'args' => 
          array (
          ),
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'A method that does nothing.',
          ),
          'modifiers' => 
          array (
            0 => 'public',
          ),
          'name' => 'one',
          'body' => '',
          'declaration' => 'public function one()',
        ),
        1 => 
        array (
          'type' => 'method',
          'args' => 
          array (
          ),
          'modifiers' => 
          array (
            0 => 'public',
          ),
          'name' => 'two',
          'body' => '',
          'declaration' => 'public function two()',
        ),
      ),
    ),
  ),
);
?>
