<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Grammars/BashGrammar.php  
  
# class Tlf\Lexer\Test\BashGrammar  
  
  
## Constants  
  
## Properties  
- `protected $thingies = [  
                'ws_dblock_func_cmnt'=>[  
            'start'=>'bash',  
            'input_file'=>'test/input/bash/ws_dblock_func_cmnt.bash',  
            'input'=>null,  
            'expect'=>[  
                'function'=>  
                [  
                    0=>[  
                        'type' => 'function',  
                        'name' => 'echo_path',  
                        'docblock' =>   
                        [  
                            'type' => 'docblock',  
                            'description' => "\n Description",  
                            'attribute' =>   
                            [  
                                0 =>   
                                [  
                                    'type' => 'attribute',  
                                    'name' => 'arg',  
                                    'description' => '$1 a path',  
                                ]  
  
                            ]  
                        ]  
                    ],  
                    1=>[  
                        'type'=>'function',  
                        'name'=>'echo_string',  
                        'docblock'=>null  
                    ]  
                ],  
                'comments'=>[  
                    0=>[  
                        'type'=>'comment',  
                        'src'=> '# comment 1',  
                        'description'=> ' comment 1',  
                    ],  
                    1=>[  
                        'type'=>'comment',  
                        'src'=> '# comment 2',  
                        'description'=> ' comment 2',  
                    ],  
                    2=>[  
                        'type'=>'comment',  
                        'src'=> '# comment 3',  
                        'description'=> ' comment 3',  
                    ]  
                ],  
            ],  
        ],  
        'comments_functions'=>[  
            'start'=>'bash',  
            'input_file'=>'test/input/bash/comments_functions.bash',  
            'input'=>null,  
            'expect'=>[  
                'comments'=>[  
                    0=>[  
                        'type'=>'comment',  
                        'src'=>'#!/usr/bin/env bash',  
                        'description'=>'!/usr/bin/env bash',  
                    ],  
                    1=>[  
                        'type'=>'comment',  
                        'src'=>'# comment one',  
                        'description'=> ' comment one',  
                    ]  
                ],  
                'function'=>[  
                    0=>[ 'type'=>'function',  
                        'name'=>'pre_comment',  
                        'docblock'=>null  
                    ],  
                    1=>[ 'type'=>'function',  
                        'name'=>'post_comment',  
                        'docblock'=>null  
                    ],  
                ],  
            ],  
        ],  
        'whitespace_docblock_function'=>[  
            'start'=>'bash',  
            'input_file'=>'test/input/bash/whitespace_docblock_function.bash',  
            'input'=>null,  
            'expect'=>[  
                'function'=>  
                [  
                    0=>[  
                        'type' => 'function',  
                        'name' => 'echo_path',  
                        'docblock' =>   
                        [  
                            'type' => 'docblock',  
                            'description' => "\n Description",  
                            'attribute' =>   
                            [  
                                0 =>   
                                [  
                                    'type' => 'attribute',  
                                    'name' => 'arg',  
                                    'description' => '$1 a path',  
                                ]  
  
                            ]  
  
                        ]  
                    ]  
                ]  
            ],  
        ],  
        'Docblock_Function'=>[  
            'start'=>'bash',  
            'input'=>"##\n# Commit all files & push to origin host\n#\n# @tip Save your project\n# @shorthand s, commit\n#"  
                    ."\nfunction core_save(){\nmsg \"Pretend save function\"\n}",  
            'expect'=>[  
                'function'=>[  
                    0=>[  
                        'type'=>'function',  
                        'name'=>'core_save',  
                        'docblock'=>[  
                            'type'=>'docblock',  
                            'description'=> "\n Commit all files & push to origin host\n",  
                            "attribute"=>[  
                                0=>[  
                                    'type'=>'attribute',  
                                    'name'=>'tip',  
                                    'description'=>'Save your project'  
                                ],  
                                1=>[  
                                    'type'=>'attribute',  
                                    'name'=>'shorthand',  
                                    'description'=>"s, commit\n",  
                                ]  
                            ],  
                        ]  
                    ],  
                ]  
            ],  
        ],  
        'Comments'=>[  
            'start'=>'comment',  
            'input'=>"var=\"abc\"\n#I am a comment\nvarb=\"def\"",  
            'expect'=>[  
                "comments"=>[  
                    0=>[  
                        'type'=>'comment',  
                        'src'=>'#I am a comment',  
                        'description'=> "I am a comment",  
                    ]  
                ],  
            ],  
        ],  
    ];` Array of inputs to lex & test  
  
## Methods   
- `public function testBashStuff()`   
- `public function testBashComment()`   
- `public function testBashDirectives()` Parse & test all tests listed in `$this->thingies[]`  
  
