<?php

namespace Tlf\Lexer\Test\Directives;

trait Vars {

    protected $_var_tests = [

        'Var.Assign.Variable'=>[
            'ast.type'=>'method_body',
            'start'=>['php_code'],
            'input'=>'$bear = $red_racoon;',
            'expect'=>[
                'statements'=>[
                    0=>[
                        'type'=>'var',
                        'line_number'=>0,
                        'name'=>'bear',
                        'set_to'=>[
                            'type'=>'var',
                            'name'=>'red_racoon',
                            'declaration'=>'$red_racoon',
                        ],
                        'declaration'=>'$bear = $red_racoon;',
                    ],
                ],
            ],
        ],

        'Var.Assign.String'=>[
            'ast.type'=>'method_body',
            'start'=>['php_code'],
            'input'=>'$bear = "barry";',
            'expect'=>[
                'statements'=>[
                    0=>[
                        'type'=>'var',
                        'line_number'=>0,
                        'name'=>'bear',
                        'value'=>'"barry"',
                        'declaration'=>'$bear = "barry";',
                    ],
                ],
            ],
        ],

    ];

    protected $old_prop_tests = [

        'Property.Assign.Concat'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>' public $global_warming = "angers"."me"."greatly";',
            'expect'=>[
                'properties'=>[
                    0=>[
                        'type'=>'property',
                        'modifiers'=>['public'],
                        'name'=>'global_warming',
                        'declaration'=>'public $global_warming = "angers"."me"."greatly";',
                        'value'=> '"angers"."me"."greatly"',
                    ],
                ],
            ],
        ],

        'Property.Two'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>' public $blm = "Yes!";private $cat="yep"; ',
            'expect'=>[
                'properties'=>[
                    0=>[
                        'type'=>'property',
                        'modifiers'=>['public'],
                        'name'=>'blm',
                        'value'=> '"Yes!"',
                        'declaration'=>'public $blm = "Yes!";',
                    ],
                    1=>[
                        'type'=>'property',
                        'modifiers'=>['private'],
                        'name'=>'cat',
                        'declaration'=>'private $cat="yep";',
                        'value'=> '"yep"',
                    ],
                ],
            ],
        ],

        'Property.Assign'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>' public $blm = "Yes!"; ',
            'expect'=>[
                'properties'=>[
                    0=>[
                        'type'=>'property',
                        'modifiers'=>['public'],
                        'name'=>'blm',
                        'declaration'=>'public $blm = "Yes!";',
                        'value'=> '"Yes!"',
                    ],
                ],
            ],
        ],

        'Property.Typed'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>' public int $blm;',
            'expect'=>[
                'properties'=>[
                    0=>[
                        'type'=>'property',
                        'modifiers'=>['public', 'int'],
                        'name'=>'blm',
                        // 'docblock'=> '',
                        'declaration'=>'public int $blm;',
                    ],
                ],
            ],
        ],

        'Property.Static'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>' static public $blm;',
            'expect'=>[
                'properties'=>[
                    0=>[
                        'type'=>'property',
                        'modifiers'=>['static','public'],
                        'name'=>'blm',
                        // 'docblock'=> '',
                        'declaration'=>'static public $blm;',
                    ],
                ],
            ],
        ],

        'Property.Simple'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>' public $blm;',
            'expect'=>[
                'properties'=>[
                    0=>[
                        'type'=>'property',
                        'modifiers'=>['public'],
                        'name'=>'blm',
                        // 'docblock'=> '',
                        'declaration'=>'public $blm;',
                    ],
                ],
            ],
        ],
    ];
}
