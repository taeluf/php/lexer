<?php

namespace Tlf\Lexer;

/**
 *
 * This is not for actual parsing yet. This is for design work. The $directives array & 'php_open' and 'namespace' are design aspects I'm interested in implementing at some point... maybe
 */
class BashGrammar extends Grammar {

    // use Bash\LanguageDirectives;
    use Bash\OtherDirectives;

    protected $expect = ['html', 'php_open'];
    /**
     * Filled by traits
     */
    public $directives;

    public $notin = [
        'asfdasdfkeyword'=>[
            // 'match'=>'/this-regex-available on php.net keywords page/',
            '__halt_compiler', 'abstract', 'and', 'array', 'as', 'break', 'callable', 'case', 'catch', 'class', 'clone', 'const', 'continue', 'declare', 'default', 'die', 'do', 'echo', 'else', 'elseif', 'empty', 'enddeclare', 'endfor', 'endforeach', 'endif', 'endswitch', 'endwhile', 'eval', 'exit', 'extends', 'final', 'for', 'foreach', 'function', 'global', 'goto', 'if', 'implements', 'include', 'include_once', 'instanceof', 'insteadof', 'interface', 'isset', 'list', 'namespace', 'new', 'or', 'print', 'private', 'protected', 'public', 'require', 'require_once', 'return', 'static', 'switch', 'throw', 'trait', 'try', 'unset', 'use', 'var', 'while', 'xor'
        ],
    ];

    public function getNamespace(){return 'bashgrammar';}

    public function __construct(){
        $this->directives = array_merge(
            // $this->_language_directives,
            $this->_other_directives,
        );
    }

    public function onLexerStart($lexer,$file,$token){
        // $lexer->addDirective($this->getDirectives(':bash')['php_open']);
    }

    public function handleDocblockEnd($lexer, $ast, $token, $directive){
        $block = $token->buffer();
        // $remLen = strlen($token->match(0));
        // $block = substr($block,0,-$remLen);
        $clean_input = preg_replace('/^\s*#+/m','',$block);
        // $block = preg_replace('/^\s*#+/m','* ',$block);
        // $block = "/**\n".$block."\n*/";

        $db_grammar = new \Tlf\Lexer\DocblockGrammar();
        $ast = $db_grammar->buildAstWithAttributes(explode("\n",$clean_input));
        // $lexer->setHead($ast);

        // $lexer->getHead()->docblocks = $lexer->getHead($ast)->docblocks ?? [];
        // $lexer->getHead()->docblocks[] = $ast;
        // $lexer->getHead()->add('dockblocks', $ast);
        $lexer->setPrevious('docblock', $ast);
    }

    public function handleFunction($lexer, $ast, $token, $directive){
        // $func_name = $token->match(1);
        $func = new \Tlf\Lexer\Ast('function');
        $func->name = $token->match(1);
        $func->docblock = $lexer->previous('docblock');
        $lexer->getHead()->add('function', $func);
    }

}
