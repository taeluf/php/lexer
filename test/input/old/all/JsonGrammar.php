<?php

namespace Tlf\Lexer\Test;

class JsonGrammar extends \Taeluf\Tester {

    public function testJsonNestedArray(){

        $data = [
            'value',
            ['yes', 'please'],
            "okay",
        ];
        $json = json_encode($data);
        // $json = json_encode($data).",\"okay\"]";

        echo "the json:\n";
        echo $json;
        echo "\nend the json:\n";

        $lexer = new \Tlf\Lexer();
        $lexer->debug = true;
        $lexer->useCache = false;
        $lexer->addGrammar($jsonGrammar = new \Tlf\Lexer\JsonGrammar());

        $ast = new \Tlf\Lexer\JsonAst("json");
        $ast->source = $json;

        
        echo "\n\n----Begin Lexing!\n\n";

        $ast = $lexer->lexAst($ast, $json);

        echo "\n\n----Lexing done!\n\n";

        unset($ast->source);
        $tree = $ast->getTree();

        print_r($tree);

        echo "\n\n----Tree Printed!\n\n";

        echo "\nJson Output array:\n\n";

        $array = $ast->getJsonData();

        echo "\nJson Output array printed!\n\n";

        $this->compare($data, $array);
    }

    public function testJsonArray(){

        $data = [
            'value',
            // 3.07,
            // true,
            "okay",
        ];
        $json = json_encode($data);


        $lexer = new \Tlf\Lexer();
        $lexer->useCache = false;
        $lexer->debug = true;
        $lexer->addGrammar($jsonGrammar = new \Tlf\Lexer\JsonGrammar());

        $ast = new \Tlf\Lexer\JsonAst("json");
        $ast->source = $json;

        
        echo "\n\n----Begin Lexing!\n\n";

        $ast = $lexer->lexAst($ast, $json);

        echo "\n\n----Lexing done!\n\n";

        unset($ast->source);
        $tree = $ast->getTree();

        print_r($tree);

        echo "\n\n----Tree Printed!\n\n";

        echo "\nJson Output array:\n\n";

        $array = $ast->getJsonData();

        echo "\nJson Output array printed!\n\n";

        $this->compare(['value','okay'], $array);
    }
}
