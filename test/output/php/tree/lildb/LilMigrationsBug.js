{
    "type": "file",
    "namespace": {
        "type": "namespace",
        "name": "Tlf",
        "declaration": "namespace Tlf;",
        "class": [
            {
                "type": "class",
                "docblock": {
                    "type": "docblock",
                    "description": "This is a minimal version of LilMigrations for debugging the properties issue"
                },
                "namespace": "Tlf",
                "fqn": "Tlf\\LilMigrationsBug",
                "name": "LilMigrationsBug",
                "declaration": "class LilMigrationsBug",
                "properties": [
                    {
                        "type": "property",
                        "modifiers": [
                            "public"
                        ],
                        "name": "prop",
                        "value": "'okay'",
                        "declaration": "public $prop = 'okay';"
                    }
                ],
                "methods": [
                    {
                        "type": "method",
                        "args": [],
                        "modifiers": [
                            "public"
                        ],
                        "name": "ok",
                        "body": "function() use ($file){\n    return;\n};\n$abc = 'def';",
                        "declaration": "public function ok()"
                    }
                ]
            }
        ]
    }
}