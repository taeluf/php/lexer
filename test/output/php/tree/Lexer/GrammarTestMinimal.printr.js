Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Tlf\Lexer\Test\Main
            [declaration] => namespace Tlf\Lexer\Test\Main;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [docblock] => Array
                                (
                                    [type] => docblock
                                    [description] => Test features of the grammar class, not lexing, and not any language-specific grammars
                                )

                            [namespace] => Tlf\Lexer\Test\Main
                            [fqn] => Tlf\Lexer\Test\Main\Grammar
                            [name] => Grammar
                            [extends] => \Tlf\Tester
                            [declaration] => class Grammar extends \Tlf\Tester
                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Get directives as they would be defined & those same directives as they would be after normaliztion

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => test
                                                                    [description] => 
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => multi dimensional array
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => child
                                                                    [description] => .index1 is the input directive, as one would define it
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => child
                                                                    [description] => .index2 is the expected output directive, as returned from the normalize method
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => getNormalizeDirectives
                                            [body] => 
                                            [declaration] => public function getNormalizeDirectives()
                                        )

                                )

                        )

                )

        )

)
