Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Tlf
            [declaration] => namespace Tlf;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [docblock] => Array
                                (
                                    [type] => docblock
                                    [description] => A lil tiny database class
                                )

                            [namespace] => Tlf
                            [fqn] => Tlf\LilDb
                            [name] => LilDb
                            [declaration] => class LilDb
                            [properties] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                    [1] => \PDO
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => a pdo instance
                                                )

                                            [name] => pdo
                                            [declaration] => public \PDO $pdo;
                                        )

                                )

                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => user
                                                            [declaration] => string $user
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => password
                                                            [declaration] => string $password
                                                        )

                                                    [2] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => db
                                                            [declaration] => string $db
                                                        )

                                                    [3] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => host
                                                            [value] => 'localhost'
                                                            [declaration] => $host='localhost'
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Convenience method to initialize with pdo
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => Tlf\LilDb
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => static
                                                    [1] => public
                                                )

                                            [name] => new
                                            [body] => $pdo = new \PDO("mysql:dbname=${db};host=${host}", $user, $password);
$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
$ldb = new static($pdo);
return $ldb;
                                            [declaration] => static public function new(string $user, string $password, string $db, $host='localhost')
                                        )

                                    [1] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => dbName
                                                            [value] => ':memory:'
                                                            [declaration] => string $dbName = ':memory:'
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Convenience method to initialize sqlite db in memory
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => Tlf\LilDb
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => static
                                                    [1] => public
                                                )

                                            [name] => sqlite
                                            [body] => $pdo = new \PDO('sqlite:'.$dbName);
$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
$ldb = new static($pdo);
return $ldb;
                                            [declaration] => static public function sqlite(string $dbName = ':memory:')
                                        )

                                    [2] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => dbName
                                                            [value] => ':memory:'
                                                            [declaration] => $dbName = ':memory:'
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Convenience method to initialize mysql db in memory
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => static
                                                    [1] => public
                                                )

                                            [name] => mysql
                                            [body] => $pdo = new \PDO('mysql:'.$dbName);
$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
$ldb = new static($pdo);
return $ldb;
                                            [declaration] => static public function mysql($dbName = ':memory:')
                                        )

                                    [3] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => \PDO
                                                                )

                                                            [name] => pdo
                                                            [declaration] => \PDO $pdo
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Initialize with a db handle
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $pdo a pdo instance
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __construct
                                            [body] => $this->pdo = $pdo;
                                            [declaration] => public function __construct(\PDO $pdo)
                                        )

                                    [4] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => tableName
                                                            [declaration] => string $tableName
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => colDefinitions
                                                            [declaration] => array $colDefinitions
                                                        )

                                                    [2] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => bool
                                                                )

                                                            [name] => recreateIfExists
                                                            [value] => false
                                                            [declaration] => bool $recreateIfExists=false
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Create a new table if it doesn't exist.

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param2
                                                                    [description] => $colDefinitions array of columns like: `['col_name'=>'VARCHAR(80)', 'col_two'=> 'integer']`
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param3
                                                                    [description] => $recreateIfExists true/false to include `DROP TABLE IF EXISTS table_name`
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => create
                                            [body] => $colStatements = [];
foreach ($colDefinitions as $col => $definition){
    $statement = '`'.$col.'` '. $definition;
    $colStatements[] = $statement;
}
$colsSql = implode(", ", $colStatements);
$drop = $recreateIfExists ? "DROP TABLE IF EXISTS `{$tableName}`;\n" : '';
$sql =
<<<SQL
    {$drop}
    CREATE TABLE IF NOT EXISTS `{$tableName}`
    (
    {$colsSql}
    )
    ;
SQL;
$this->exec($sql);
                                            [declaration] => public function create(string $tableName, array $colDefinitions, bool $recreateIfExists=false)
                                        )

                                    [5] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => sql
                                                            [declaration] => string $sql
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => binds
                                                            [value] => []
                                                            [declaration] => array $binds=[]
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Execute an Sql statement & get rows back
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => throws
                                                                    [description] => if the statement fails to prepare
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => query
                                            [body] => $pdo = $this->pdo;
$stmt = $pdo->prepare($sql);
if ($stmt===false){
    $error = var_export($pdo->errorInfo(),true);
    throw new \Exception("Sql problem: \n".$error."\n\n");
}
$stmt->execute($binds);
$rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
return $rows;
                                            [declaration] => public function query(string $sql, array $binds=[])
                                        )

                                    [6] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => tableName
                                                            [declaration] => string $tableName
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => whereCols
                                                            [value] => []
                                                            [declaration] => array $whereCols=[]
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Get rows from a table with the given $whereCols
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => select
                                            [body] => $sql = "SELECT * FROM `${tableName}` ";
$binds = static::keysToBinds($whereCols);
if (count($whereCols)>0){
    $whereStr = "Where ".static::whereSqlFromCols($whereCols);
    $sql .= $whereStr;
}
$rows = $this->query($sql, $binds);
return $rows;
                                            [declaration] => public function select(string $tableName, array $whereCols=[])
                                        )

                                    [7] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => table
                                                            [declaration] => string $table
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => row
                                                            [declaration] => array $row
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Insert a row into the database
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => throws
                                                                    [description] => Exception if the insert fails
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => the newly inserted id
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => insert
                                            [body] => $pdo = $this->pdo;
$cols = [];
$binds = [];
foreach ($row as $key=>$value){
    $cols[] = $key;
    $binds[":{$key}"] = $value;
}
$colsStr = '`'.implode('`, `',$cols).'`';
$bindsStr = implode(', ', array_keys($binds));
$query = "INSERT INTO `${table}`(${colsStr})
        VALUES (${bindsStr})
    ";
$stmt = $pdo->prepare($query);
if ($stmt===false){
    throw new \Exception("Could not insert values into databse.". print_r($pdo->errorInfo(),true));
}
$stmt->execute($binds);
if ($stmt->errorCode()!=='00000'){
    print_r($stmt->errorInfo());
    throw new \Exception("There was an error inserting data");
}
return $pdo->lastInsertId();
                                            [declaration] => public function insert(string $table, array $row)
                                        )

                                    [8] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => table
                                                            [declaration] => string $table
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => rowSet
                                                            [declaration] => array $rowSet
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => insertAll
                                            [body] => foreach ($rowSet as $row){
    $lastInsertId = $this->insert($table, (array)$row);
}
return $lastInsertId;
                                            [declaration] => public function insertAll(string $table, array $rowSet)
                                        )

                                    [9] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => table
                                                            [declaration] => string $table
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => newRowValues
                                                            [declaration] => array $newRowValues
                                                        )

                                                    [2] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => idColumnName
                                                            [value] => 'id'
                                                            [declaration] => string $idColumnName='id'
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Update an existing row. Shorthand for `updateWhere()` with the id column set as the where values.
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => update
                                            [body] => return $this->updateWhere($table, $newRowValues, [$idColumnName=>$newRowValues[$idColumnName]]);
                                            [declaration] => public function update(string $table, array $newRowValues, string $idColumnName='id')
                                        )

                                    [10] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => table
                                                            [declaration] => string $table
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => newRowValues
                                                            [declaration] => array $newRowValues
                                                        )

                                                    [2] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => whereVals
                                                            [declaration] => array $whereVals
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => 
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $whereVals To update ALL rows, pass `[]`
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => updateWhere
                                            [body] => $valueBinds = [];
$setSql = [];
foreach ($newRowValues as $col=>$value){
    $valueBinds[$bindKey=':'.$col.'_value'] = $value;
    $setSql[] = "`$col` = $bindKey";
}
$setSql = implode(",\n", $setSql);
$whereSql = static::whereSqlFromCols($whereVals);
if (strlen(trim($whereSql))>0)$whereSql = "WHERE\n${whereSql}";
$sql = <<<SQL
    UPDATE `${table}`
    SET $setSql
    ${whereSql}
SQL;
$binds = array_merge($valueBinds, $whereVals);
$binds  = static::keysToBinds($binds);
$this->execute($sql,$binds);
                                            [declaration] => public function updateWhere(string $table, array $newRowValues, array $whereVals)
                                        )

                                    [11] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => table
                                                            [declaration] => string $table
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => whereCols
                                                            [declaration] => array $whereCols
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Delete rows from a table
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => true if any rows were deleted. false otherwise
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => delete
                                            [body] => $sql = static::whereSqlFromCols($whereCols);
if ($sql!=null)$sql = 'WHERE '.$sql;
$sql = "DELETE FROM `${table}` ${sql}";
$stmt = $this->execute($sql, $whereCols);
// var_dump($stmt->errorCode());
// exit;
// var_dump($stmt->rowCount());
// exit;
if ($stmt->errorCode()=='00000'
    &&$stmt->rowCount()>0)return true;
return false;
// return $stmt;
                                            [declaration] => public function delete(string $table, array $whereCols)
                                        )

                                    [12] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => sql
                                                            [declaration] => string $sql
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => binds
                                                            [value] => []
                                                            [declaration] => array $binds=[]
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Execute an Sql statement & get a PDOStatement back
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => throws
                                                                    [description] => if the statement fails to prepare
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => PDOStatement
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => execute
                                            [body] => $pdo = $this->pdo;
$stmt = $pdo->prepare($sql);
if ($stmt===false){
    $error = var_export($pdo->errorInfo(),true);
    throw new \Exception("Sql problem: \n".$error."\n\n");
}
$stmt->execute($binds);
return $stmt;
                                            [declaration] => public function execute(string $sql, array $binds=[])
                                        )

                                    [13] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => sql
                                                            [declaration] => string $sql
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => binds
                                                            [value] => []
                                                            [declaration] => array $binds=[]
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Alias for `execute()`
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => PDOStatement
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => exec
                                            [body] => return $this->execute($sql, $binds);
                                            [declaration] => public function exec(string $sql, array $binds=[])
                                        )

                                    [14] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => get the pdo object
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => getPdo
                                            [body] => return $this->pdo;
                                            [declaration] => public function getPdo()
                                        )

                                    [15] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => get the pdo object
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => pdo
                                            [body] => return $this->pdo;
                                            [declaration] => public function pdo()
                                        )

                                    [16] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => columns
                                                            [declaration] => array $columns
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Convert key=>value array into a 'WHERE' sql.

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $columns `['key'=>$val, ':key2'=>$val]`. `$val` can be string, array, or numeric.
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string sql for a WHERE statement. Does not include `WHERE`
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => exampleOutput
                                                                    [description] => : `key = :val1 AND key2 LIKE :val2, AND key3 IN (:val3_1,:val3_2)`.
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => static
                                                    [1] => public
                                                )

                                            [name] => whereSqlFromCols
                                            [body] => $binds = static::keysToBinds($columns);
//generate sql
$pieces = [];
$copy = $binds;
foreach ($copy as $k=>$v){
    $col = substr($k,1);
    if (is_string($v)){
        $pieces[] = "`$col` LIKE $k";
    } else if (is_array($v)){
        unset($binds[$k]);
        $inList = [];
        foreach ($v as $index=>$inValue){
            $inKey = $k.$index;
            $binds[$inKey] = $inValue;
            $inList[] = $inKey;
        }
        $pieces[] = "`$col` IN (".implode(', ',$inList).")";
    } else {
        $pieces[] = "`$col` = $k";
    }
}
$sql = implode(' AND ', $pieces);
return $sql;
                                            [declaration] => static public function whereSqlFromCols(array $columns)
                                        )

                                    [17] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => array
                                                                )

                                                            [name] => keyedValues
                                                            [declaration] => array $keyedValues
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Convert an array `['key'=>$val, ':key2'=>$val]` into binds: `[':key'=>$val, ':key2'=>$val]`.

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => array where keys are prefixed with a colon (:)
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => static
                                                    [1] => public
                                                )

                                            [name] => keysToBinds
                                            [body] => $binds = [];
foreach ($keyedValues as $k=>$v){
    if (!is_string($k)){
        $binds[] = $v;
    } else if (substr($k,0,1)==':'){
        $binds[$k] = $v;
    } else {
        $binds[':'.$k] = $v;
    }
}
return $binds;
                                            [declaration] => static public function keysToBinds(array $keyedValues)
                                        )

                                )

                        )

                )

        )

)
