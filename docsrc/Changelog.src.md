# ChangeLog

## Other Notes
- 2023-12-23: Create branch v0.8-programming-language. Idea: Focus the Lexer around an own-built programming language. Currently the language is written as Directives & the stdlib is in php. This library is centered around the Lexer. Instead, this library could be centered around the programming language.
- 2023-11-21: Cleaned up the repo, moved files around
- 2023-11-21: Added prototype outputting code from an ast

## Git Log
`git log` on @system(date, trim)`
@system(git log --pretty=format:'- %h %d %s [%cr]' --abbrev-commit)
