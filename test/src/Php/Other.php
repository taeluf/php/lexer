<?php

namespace Tlf\Lexer\Test\Directives;

trait Other {
/**
 *
 * This fails because \' is not being handled ... I think 
 *
 */
protected $_other_tests = [

        // BUG: `public function someFunc(string $namespae){}`  - the `$namespace` breaks it. `$znamespace` is fine. `nzamespace` is fine. `$this->namepsace` in the function body is fine. `$namespace` in the function body is fine.
        'Bug.namespace'=>[
            // 'expect_failure'=>true,
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>
                <<<PHP
                    public function namespace_add(string \$namespace) {
                        \$this->namespace[] = \$namespace;
                    } 
                PHP,
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[],
                        'modifiers'=>['public'],
                        'name'=>'namespace_add',
                        'return_types'=>[],
                        'body'=>"\$this->namespace[] = \$namespace;",
                        'declaration'=>'public function namespace_add(string $namespace)',
                    ],
                ],
            ],
        ],
    
        'Function.Anon'=>[
            // 'expect_failure'=>true,
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>
                <<<PHP
                    function protect_womens_rights() use (\$abc){
                        return;
                    } 
                PHP,
            'expect'=>[
                'functions'=>[
                    0=>[
                        'type'=>'function',
                        'args'=>[],
                        'name'=>'protect_womens_rights',
                        // 'return_types'=>[],

                        'use_vars'=>[
                            0=>[
                                'type'=>'use_vars',
                                'args'=>[
                                    0=>['type'=>'arg', 'name'=>'abc', 'declaration'=>'$abc']
                                ],
                            ]
                        ],
                        'body'=>"return;",
                        'declaration'=>'function protect_womens_rights() use ($abc)',
                    ],
                ],
            ],
        ],
    
        'Function.SimpleBody'=>[
            // 'expect_failure'=>true,
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>
                <<<PHP
                    function is_the_us_imperialist():bool {
                        \$var = new \Value();
                        return true;
                    } 
                PHP,
            'expect'=>[
                'functions'=>[
                    0=>[
                        'type'=>'function',
                        'args'=>[],
                        'name'=>'is_the_us_imperialist',
                        'return_types'=>['bool'],
                        'body'=>"\$var = new \\Value();\nreturn true;",
                        'declaration'=>'function is_the_us_imperialist():bool',
                    ],
                ],
            ],
        ],
        'Const.Simple'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'const blm = "This"."is".\'a\'."const";',
            'expect'=>[
                'const'=>[
                    0=>[
                        'type'=>'const',
                        // 'modifiers'=>['public'],
                        'name'=>'blm',
                        // 'docblock'=> '',
                        'declaration'=>'const blm = "This"."is".\'a\'."const";',
                        'value'=>'"This"."is".\'a\'."const"',
                    ],
                ],
            ],
        ],
];
}
