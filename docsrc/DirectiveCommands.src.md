# Grammar Commands
These commands are available for your directives to execute. See an example at @see_file(docs/GrammarExample.md). See instructions for writing a grammar at @see_file(docs/GrammarWriting.md)

In the internals, there are two types of commands. 
- switch/case commands, which generally have a simple or small implementation. 
- mapped commands, which point a command to a method (rather than the switch-case)
    
## Mapped commands
The key points to a method on the `$lexer` instance, but they're written in the `MappedMethods` trait. See @see_file(code/Lexer/MappedMethods.php)
```php
@import(Commands.MethodMap)
```

## `switch/case` commands 
These are the exact implementations of the commands from @see_file(code/Lexer/Instructions.php)
```php
@import(Commands.SwitchCase)
```
